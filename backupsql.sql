/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50516
Source Host           : localhost:3306
Source Database       : dbpim

Target Server Type    : MYSQL
Target Server Version : 50516
File Encoding         : 65001

Date: 2014-07-18 06:44:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `newtrackvisitor`
-- ----------------------------
DROP TABLE IF EXISTS `newtrackvisitor`;
CREATE TABLE `newtrackvisitor` (
  `trackuser` varchar(50) NOT NULL,
  `trackip` varchar(255) NOT NULL,
  `trackcontinent` varchar(255) NOT NULL,
  `trackregion` varchar(255) NOT NULL,
  `trackcountry` varchar(255) NOT NULL,
  `trackpage` text,
  `trackroute` text,
  `tracksafe` text,
  `trackquery` text,
  `trackreferrer` text NOT NULL,
  `trackos` varchar(255) NOT NULL,
  `trackhost` varchar(255) NOT NULL,
  `trackbrowser` varchar(255) NOT NULL,
  `trackdatetime` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of newtrackvisitor
-- ----------------------------

-- ----------------------------
-- Table structure for `t_agama`
-- ----------------------------
DROP TABLE IF EXISTS `t_agama`;
CREATE TABLE `t_agama` (
  `id_agama` tinyint(4) NOT NULL AUTO_INCREMENT,
  `agama` char(15) DEFAULT NULL,
  `urutan` tinyint(1) DEFAULT NULL,
  `publish` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_agama`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_agama
-- ----------------------------
INSERT INTO `t_agama` VALUES ('1', 'ISLAM', '1', '1');
INSERT INTO `t_agama` VALUES ('2', 'KRISTEN', '2', '1');
INSERT INTO `t_agama` VALUES ('3', 'KATOLIK', '3', '1');
INSERT INTO `t_agama` VALUES ('4', 'HINDU', '4', '1');
INSERT INTO `t_agama` VALUES ('5', 'BUDHA', '5', '1');
INSERT INTO `t_agama` VALUES ('6', 'KONGHUCHU', '6', '1');
INSERT INTO `t_agama` VALUES ('7', 'LAINNYA', '7', '1');

-- ----------------------------
-- Table structure for `t_badan_usaha`
-- ----------------------------
DROP TABLE IF EXISTS `t_badan_usaha`;
CREATE TABLE `t_badan_usaha` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `kode` char(5) DEFAULT NULL,
  `keterangan` char(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_badan_usaha
-- ----------------------------
INSERT INTO `t_badan_usaha` VALUES ('1', '1', 'PT');
INSERT INTO `t_badan_usaha` VALUES ('2', '2', 'CV');
INSERT INTO `t_badan_usaha` VALUES ('3', '3', 'UD');
INSERT INTO `t_badan_usaha` VALUES ('4', '4', 'KOPERASI');
INSERT INTO `t_badan_usaha` VALUES ('5', '5', 'YAYASAN');
INSERT INTO `t_badan_usaha` VALUES ('6', '6', 'INSTANSI PEMERINTAH');
INSERT INTO `t_badan_usaha` VALUES ('7', '999', 'LAINNYA ...');
INSERT INTO `t_badan_usaha` VALUES ('8', '0', 'TIDAK ADA / KOSONG');

-- ----------------------------
-- Table structure for `t_bank`
-- ----------------------------
DROP TABLE IF EXISTS `t_bank`;
CREATE TABLE `t_bank` (
  `id_bank` char(15) NOT NULL,
  `nama_bank` char(25) NOT NULL,
  `cabang` char(25) NOT NULL,
  `alamat` char(100) NOT NULL,
  `blok` char(5) DEFAULT NULL,
  `no` char(5) DEFAULT NULL,
  `rt` char(5) DEFAULT NULL,
  `rw` char(5) DEFAULT NULL,
  `kel` char(25) DEFAULT NULL,
  `kec` char(25) DEFAULT NULL,
  `kota` char(25) DEFAULT NULL,
  `prov` char(25) DEFAULT NULL,
  `kodepos` char(7) DEFAULT NULL,
  `telp` char(25) DEFAULT NULL,
  `fax` char(25) DEFAULT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '1',
  `userid` char(25) NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_bank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_bank
-- ----------------------------
INSERT INTO `t_bank` VALUES ('1393110405', 'BTN SYARIAH', 'SMG', 'JL', '', '', '', '', '', '', 'SEMARANG', '', '', '024', '024', '1', 'admin', '2014-05-03 13:33:16');
INSERT INTO `t_bank` VALUES ('1399100494', 'BUKOPIN', 'SEMARANG', 'JL', '', '', '', '', '', '', 'SEMARANG', '', '', '024', '024', '1', 'admin', '2014-05-03 14:01:34');

-- ----------------------------
-- Table structure for `t_bk_kas`
-- ----------------------------
DROP TABLE IF EXISTS `t_bk_kas`;
CREATE TABLE `t_bk_kas` (
  `idbkkas` char(15) NOT NULL,
  `jenis_kas` char(15) DEFAULT NULL,
  `id_proyek` char(15) DEFAULT NULL,
  `id_penjualan` char(15) DEFAULT NULL,
  `id_faktur` char(15) DEFAULT NULL,
  `tgl_bk` date DEFAULT '0000-00-00',
  `no_bk` char(25) DEFAULT NULL,
  `kd_rekening` char(25) DEFAULT NULL,
  `kd_lawan` char(25) DEFAULT NULL,
  `memo` text,
  `status` smallint(1) NOT NULL DEFAULT '0',
  `sts_jual` smallint(1) NOT NULL DEFAULT '1',
  `tglinput` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userid` char(25) DEFAULT NULL,
  PRIMARY KEY (`idbkkas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_bk_kas
-- ----------------------------
INSERT INTO `t_bk_kas` VALUES ('1401888861947', 'piu', '1393089671', '1400952212', null, '2014-06-04', '401000', '1400733645', null, 'PIUTANG a.n  HERMAN WAHYU PRATOMO ', '1', '0', '2014-06-04 20:34:41', 'admin');
INSERT INTO `t_bk_kas` VALUES ('1401891777927', 'piu', '1393089671', '1400952401', null, '2014-06-04', '401001', '1400733494', null, 'PIUTANG a.n  AJI JOKO LELONO ', '1', '0', '2014-06-04 21:23:27', 'admin');
INSERT INTO `t_bk_kas` VALUES ('1401938338464', 'pp', '1393089671', '1400952212', null, '2014-06-05', '501000', '1400301797', null, 'PEMBAYARAN a.n  HERMAN WAHYU PRATOMO ', '1', '0', '2014-06-05 10:20:27', 'admin');
INSERT INTO `t_bk_kas` VALUES ('1401941114306', 'pp', '1393089671', '1400952212', null, '2014-06-05', '501001', '1400301797', null, 'PEMBAYARAN a.n  HERMAN WAHYU PRATOMO ', '1', '0', '2014-06-05 11:05:39', 'admin');
INSERT INTO `t_bk_kas` VALUES ('1402033807267', 'piu', '1393089671', '1402033782', null, '2014-06-06', '401002', '1400733494', null, 'PIUTANG a.n  EIRENE YUSINTA ', '1', '0', '2014-06-06 12:50:32', 'admin');
INSERT INTO `t_bk_kas` VALUES ('1402033910144', 'pp', '1393089671', '1402033782', null, '2014-06-06', '501002', '1400301797', null, 'PEMBAYARAN a.n  EIRENE YUSINTA ', '1', '0', '2014-06-06 12:52:21', 'admin');
INSERT INTO `t_bk_kas` VALUES ('1402543275026', 'km01', '1393090522', null, null, '2014-06-12', '1', '1400301797', null, '', '1', '1', '2014-06-12 10:21:30', 'admin');
INSERT INTO `t_bk_kas` VALUES ('1402545318905', 'kk01', '1393090522', null, null, '2014-06-12', '1', '1400301797', null, '', '1', '1', '2014-06-12 10:55:41', 'admin');
INSERT INTO `t_bk_kas` VALUES ('1402547286054', 'bj01', '1393090522', null, null, '2014-06-12', '1', null, null, '', '1', '1', '2014-06-12 11:28:22', 'admin');
INSERT INTO `t_bk_kas` VALUES ('1402591683244', 'hutang', '1393089671', '1400952401', null, '2014-06-12', '1001', '1400301797', null, 'HUTANG KEPADA KONSUMEN a.n  AJI JOKO LELONO ', '1', '0', '2014-06-12 23:48:54', 'admin');
INSERT INTO `t_bk_kas` VALUES ('14026005050', 'retur', '1393089671', '1402033782', null, '2014-06-12', '00008', '1402418227', null, 'PENGEMBALIAN UANG TITIPAN a.n EIRENE YUSINTA', '1', '1', '2014-06-13 02:15:05', null);
INSERT INTO `t_bk_kas` VALUES ('14026005051', 'potongan', '1393089671', '1402033782', null, '2014-06-12', '00008', '1402418227', null, 'BIAYA ADMINISTRASI PENGEMBALIAN a.n EIRENE YUSINTA', '1', '1', '2014-06-13 02:15:05', null);
INSERT INTO `t_bk_kas` VALUES ('1404421889481', 'km01', '1393090522', null, null, '2014-07-04', 'km11', '1400301797', null, 'dropping kas', '1', '1', '2014-07-04 04:11:55', 'admin');

-- ----------------------------
-- Table structure for `t_bk_titipan`
-- ----------------------------
DROP TABLE IF EXISTS `t_bk_titipan`;
CREATE TABLE `t_bk_titipan` (
  `idbk` char(15) NOT NULL,
  `idpenjualan` char(15) NOT NULL,
  `tglbk` date NOT NULL,
  `nobk` char(25) NOT NULL,
  `rek` char(25) DEFAULT NULL,
  `reklawan` char(25) DEFAULT NULL,
  `tglkuitansi` date NOT NULL,
  `userid` char(25) NOT NULL,
  `update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idbk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_bk_titipan
-- ----------------------------
INSERT INTO `t_bk_titipan` VALUES ('1398686882', '1398474113', '2014-04-29', '29042014', '123456789', '0987654321', '2014-04-29', 'admin', '2014-04-28 19:08:02');
INSERT INTO `t_bk_titipan` VALUES ('1398687114', '1398474113', '2014-04-30', '30042014', '1234567890', '0987654321', '2014-04-30', 'admin', '2014-04-28 19:11:54');
INSERT INTO `t_bk_titipan` VALUES ('1398687251', '1398474113', '2014-05-01', '01052014', '', '', '2014-05-01', 'admin', '2014-04-28 19:14:11');
INSERT INTO `t_bk_titipan` VALUES ('1398689534', '1398539777', '2014-04-28', '28042014', '123', '321', '2014-04-28', 'admin', '2014-04-28 19:52:14');
INSERT INTO `t_bk_titipan` VALUES ('1398690357', '1398474113', '2014-04-25', '25042014', '', '', '2014-04-25', 'admin', '2014-04-28 20:05:57');

-- ----------------------------
-- Table structure for `t_data_kas`
-- ----------------------------
DROP TABLE IF EXISTS `t_data_kas`;
CREATE TABLE `t_data_kas` (
  `id` char(15) NOT NULL,
  `idbkkas` char(15) NOT NULL,
  `kode` char(15) NOT NULL,
  `nokwitansi` char(25) DEFAULT NULL,
  `qty` char(5) DEFAULT NULL,
  `stn` char(15) DEFAULT NULL,
  `memo` text,
  `debet` int(11) NOT NULL DEFAULT '0',
  `kredit` int(11) NOT NULL DEFAULT '0',
  `id_penjualan` char(15) DEFAULT NULL,
  `status` smallint(1) DEFAULT '0',
  `sts_piutang` smallint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_data_kas
-- ----------------------------
INSERT INTO `t_data_kas` VALUES ('1401888861947', '1401888861947', '1400733645', null, null, null, null, '0', '11000000', null, '0', '1');
INSERT INTO `t_data_kas` VALUES ('1401890932', '1401888861947', '1400683382', '', '', '0', 'uang muka sebelum proses NOTARIS', '10000000', '0', null, '0', '0');
INSERT INTO `t_data_kas` VALUES ('1401891114', '1401888861947', '1400683412', '', '10', '100000', 'KLT 10m2', '1000000', '0', null, '0', '0');
INSERT INTO `t_data_kas` VALUES ('1401891777927', '1401891777927', '1400733494', null, null, null, null, '0', '8000000', null, '0', '1');
INSERT INTO `t_data_kas` VALUES ('1401891859', '1401891777927', '1400683382', '', '', '0', '', '8000000', '0', null, '0', '0');
INSERT INTO `t_data_kas` VALUES ('1401938338464', '1401938338464', '1400301797', null, null, null, null, '4000000', '0', null, '0', '1');
INSERT INTO `t_data_kas` VALUES ('1401939652', '1401938338464', '1400683382', '', null, null, '', '0', '3000000', null, '0', '0');
INSERT INTO `t_data_kas` VALUES ('1401941114306', '1401941114306', '1400301797', null, null, null, null, '2000000', '0', null, '0', '1');
INSERT INTO `t_data_kas` VALUES ('1401941164', '1401941114306', '1400683412', '', null, null, '', '0', '2000000', null, '0', '0');
INSERT INTO `t_data_kas` VALUES ('1401941647', '1401938338464', '1400683382', '', null, null, '', '0', '1000000', null, '0', '0');
INSERT INTO `t_data_kas` VALUES ('1402033807267', '1402033807267', '1400733494', null, null, null, null, '0', '6000000', null, '0', '1');
INSERT INTO `t_data_kas` VALUES ('1402033871', '1402033807267', '1400683382', '', '', '0', '', '6000000', '0', null, '0', '0');
INSERT INTO `t_data_kas` VALUES ('1402033910144', '1402033910144', '1400301797', null, null, null, null, '6000000', '0', null, '0', '1');
INSERT INTO `t_data_kas` VALUES ('1402035744', '1402033910144', '1400683382', '', null, null, '', '0', '6000000', null, '0', '0');
INSERT INTO `t_data_kas` VALUES ('1402543275026', '1402543275026', '1400301797', null, null, null, null, '3350000', '0', null, '0', '1');
INSERT INTO `t_data_kas` VALUES ('1402544328', '1402543275026', '1402544026', '', '', '0', '', '0', '2500000', '', '0', '1');
INSERT INTO `t_data_kas` VALUES ('1402544620', '1402543275026', '1402544026', '', '', '', '', '0', '650000', '', '0', '1');
INSERT INTO `t_data_kas` VALUES ('1402544635', '1402543275026', '1402544026', '', '', '0', '', '0', '200000', '', '0', '1');
INSERT INTO `t_data_kas` VALUES ('1402545318905', '1402545318905', '1400301797', null, null, null, null, '0', '12300', null, '0', '1');
INSERT INTO `t_data_kas` VALUES ('1402546477', '1402545318905', '1400685877', '', '1', '5000', 'Beli Buku Tulis u/ Security', '5000', '0', '', '0', '1');
INSERT INTO `t_data_kas` VALUES ('1402546555', '1402545318905', '1400685877', '', '', '', 'Fc Form Berkas', '7300', '0', '', '0', '1');
INSERT INTO `t_data_kas` VALUES ('1402548265', '1402547286054', '1402548224', '', '', '0', 'Tagihan Semen ', '0', '9850000', '', '1', '1');
INSERT INTO `t_data_kas` VALUES ('1402548507', '1402547286054', '1400301797', '', '', '', '', '8850000', '0', '', '0', '1');
INSERT INTO `t_data_kas` VALUES ('1402548543', '1402547286054', '1400686201', '', '', '', 'Dr Bu Kris', '1000000', '0', '', '0', '1');
INSERT INTO `t_data_kas` VALUES ('1402591683244', '1402591683244', '1400301797', null, null, null, null, '2000000', '0', null, '0', '1');
INSERT INTO `t_data_kas` VALUES ('1402591749', '1402591683244', '1402590774', '', null, null, '', '0', '2000000', null, '0', '0');
INSERT INTO `t_data_kas` VALUES ('14026005050', '14026005050', '1402418227', null, null, null, null, '5000000', '0', null, '0', '1');
INSERT INTO `t_data_kas` VALUES ('14026005051', '14026005051', '1400301797', null, null, null, null, '1000000', '0', null, '0', '1');
INSERT INTO `t_data_kas` VALUES ('140260050599', '14026005050', '1400301797', null, null, null, null, '0', '5000000', null, '0', '1');
INSERT INTO `t_data_kas` VALUES ('1402600505999', '14026005051', '1402417836', null, null, null, null, '0', '1000000', null, '0', '1');
INSERT INTO `t_data_kas` VALUES ('1404421889481', '1404421889481', '1400301797', null, null, null, null, '5000000', '0', null, '0', '1');
INSERT INTO `t_data_kas` VALUES ('1404421992', '1404421889481', '1402544026', '122333', '', '', 'dropping kas pak firman', '0', '5000000', '', '0', '1');

-- ----------------------------
-- Table structure for `t_data_pendidikan`
-- ----------------------------
DROP TABLE IF EXISTS `t_data_pendidikan`;
CREATE TABLE `t_data_pendidikan` (
  `id` char(25) NOT NULL,
  `pendidikan` char(25) DEFAULT NULL,
  `publish` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_data_pendidikan
-- ----------------------------
INSERT INTO `t_data_pendidikan` VALUES ('1389983352', 'SD / MI', '1');
INSERT INTO `t_data_pendidikan` VALUES ('1389983365', 'SMP / SLTP', '1');
INSERT INTO `t_data_pendidikan` VALUES ('1389983384', 'SMA / SLTA', '1');
INSERT INTO `t_data_pendidikan` VALUES ('1389983400', 'DIPLOMA', '1');
INSERT INTO `t_data_pendidikan` VALUES ('1389983423', 'S1', '1');
INSERT INTO `t_data_pendidikan` VALUES ('1389983439', 'S2', '1');
INSERT INTO `t_data_pendidikan` VALUES ('1389983455', 'S3', '1');

-- ----------------------------
-- Table structure for `t_data_pengguna`
-- ----------------------------
DROP TABLE IF EXISTS `t_data_pengguna`;
CREATE TABLE `t_data_pengguna` (
  `iduser` int(11) NOT NULL,
  `nama` char(50) NOT NULL,
  `userid` char(25) NOT NULL,
  `passid` char(25) NOT NULL,
  `passenc` char(50) NOT NULL,
  `aksesuser` text,
  `visible` smallint(1) NOT NULL DEFAULT '1',
  `publish` smallint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_data_pengguna
-- ----------------------------
INSERT INTO `t_data_pengguna` VALUES ('1398072416', 'ADMIN KRISTI', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', '1,48,49,50,51,58,59,60,62,79,81,82,83,86,95,56,87,93,94,98,99,63,89,91,92,104,105,106,96,97,90,100,101,102,103,111,112,113,114,108,109,107,110,115,116,122,117,118,119,120,121,125,126,127,128,129,130,131', '1', '1');
INSERT INTO `t_data_pengguna` VALUES ('1399264129', 'PUTRANTI', 'backoffice', '11a', '1c222c9181f2dfcc08ca42e869cf88c8', '1,48,49,50,51,58,59,60,62,79,56,87,93,94,63,90,100', '1', '1');
INSERT INTO `t_data_pengguna` VALUES ('1399264285', 'IRENA', 'finace', '11b', 'faecbd2dae425cfd3e4b4e5a9ecfdc84', '63,89,91,92,104,105,96,97,90,100,101,111,112,113,114,108,109,107,110', '1', '1');
INSERT INTO `t_data_pengguna` VALUES ('1399264352', 'AGUSTINUS', 'supportkpr', '13', 'c51ce410c124a10e0db5e4b97fc2af39', '56,98,99,90,100', '1', '1');

-- ----------------------------
-- Table structure for `t_data_titipan`
-- ----------------------------
DROP TABLE IF EXISTS `t_data_titipan`;
CREATE TABLE `t_data_titipan` (
  `idtitip` char(15) NOT NULL,
  `idbk` char(15) NOT NULL,
  `idkode` char(15) NOT NULL,
  `total` int(11) NOT NULL,
  `userid` char(25) NOT NULL,
  `lastedit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idtitip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_data_titipan
-- ----------------------------
INSERT INTO `t_data_titipan` VALUES ('13986868829', '1398686882', '1398615393', '10000', 'admin', '2014-04-28 19:08:02');
INSERT INTO `t_data_titipan` VALUES ('139868711411', '1398687114', '1398615511', '10000000', 'admin', '2014-04-28 19:11:54');
INSERT INTO `t_data_titipan` VALUES ('139868725111', '1398687251', '1398615511', '5000000', 'admin', '2014-04-28 19:14:11');
INSERT INTO `t_data_titipan` VALUES ('13986895349', '1398689534', '1398615511', '9000000', 'admin', '2014-04-28 19:52:14');
INSERT INTO `t_data_titipan` VALUES ('139869035710', '1398690357', '1398615805', '9000000', 'admin', '2014-04-28 20:05:57');
INSERT INTO `t_data_titipan` VALUES ('13986903579', '1398690357', '1398615511', '1000000', 'admin', '2014-04-28 20:05:57');

-- ----------------------------
-- Table structure for `t_dp_lain`
-- ----------------------------
DROP TABLE IF EXISTS `t_dp_lain`;
CREATE TABLE `t_dp_lain` (
  `id_dp` char(15) NOT NULL,
  `nama_dp` char(25) DEFAULT NULL,
  `publish` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_dp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_dp_lain
-- ----------------------------
INSERT INTO `t_dp_lain` VALUES ('1', 'TIDAK ADA', '1');
INSERT INTO `t_dp_lain` VALUES ('1382706715', 'Bapertarum', '1');
INSERT INTO `t_dp_lain` VALUES ('999', 'LAINNYA ...', '1');

-- ----------------------------
-- Table structure for `t_info_bank`
-- ----------------------------
DROP TABLE IF EXISTS `t_info_bank`;
CREATE TABLE `t_info_bank` (
  `id` char(15) NOT NULL,
  `id_penjualan` char(15) DEFAULT NULL,
  `id_bank` char(15) DEFAULT NULL,
  `id_syarat` char(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_info_bank
-- ----------------------------

-- ----------------------------
-- Table structure for `t_jabatan`
-- ----------------------------
DROP TABLE IF EXISTS `t_jabatan`;
CREATE TABLE `t_jabatan` (
  `id_jabatan` char(15) NOT NULL,
  `nama_jabatan` char(50) DEFAULT NULL,
  `publish` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_jabatan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_jabatan
-- ----------------------------
INSERT INTO `t_jabatan` VALUES ('1389941486', 'MARKETING', '1');
INSERT INTO `t_jabatan` VALUES ('1393736691', 'DIREKTUR UTAMA', '1');

-- ----------------------------
-- Table structure for `t_jenis_kas`
-- ----------------------------
DROP TABLE IF EXISTS `t_jenis_kas`;
CREATE TABLE `t_jenis_kas` (
  `id` smallint(2) NOT NULL AUTO_INCREMENT,
  `kode` char(15) NOT NULL,
  `keterangan` char(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_jenis_kas
-- ----------------------------
INSERT INTO `t_jenis_kas` VALUES ('1', 'km01', 'PENERIMAAN');
INSERT INTO `t_jenis_kas` VALUES ('2', 'kk01', 'PEMBAYARAN');
INSERT INTO `t_jenis_kas` VALUES ('3', 'bj01', 'JURNAL UMUM');
INSERT INTO `t_jenis_kas` VALUES ('4', 'piu', 'FAKTUR PENJUALAN');
INSERT INTO `t_jenis_kas` VALUES ('5', 'pp', 'PENERIMAAN PENJUALAN');
INSERT INTO `t_jenis_kas` VALUES ('6', 'retur', 'RETUR PENJUALAN');
INSERT INTO `t_jenis_kas` VALUES ('7', 'hutang', 'HUTANG KE KONSUMEN');
INSERT INTO `t_jenis_kas` VALUES ('8', 'potongan', 'BIAYA ADMINISTRASI');

-- ----------------------------
-- Table structure for `t_jenis_kelamin`
-- ----------------------------
DROP TABLE IF EXISTS `t_jenis_kelamin`;
CREATE TABLE `t_jenis_kelamin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` char(5) NOT NULL,
  `keterangan` char(15) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_jenis_kelamin
-- ----------------------------
INSERT INTO `t_jenis_kelamin` VALUES ('1', 'L', 'Laki - Laki');
INSERT INTO `t_jenis_kelamin` VALUES ('2', 'P', 'Perempuan');

-- ----------------------------
-- Table structure for `t_jenis_penjualan`
-- ----------------------------
DROP TABLE IF EXISTS `t_jenis_penjualan`;
CREATE TABLE `t_jenis_penjualan` (
  `id` tinyint(1) NOT NULL AUTO_INCREMENT,
  `id_sub` tinyint(1) DEFAULT NULL,
  `kode` char(5) DEFAULT NULL,
  `jenis_penjualan` char(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_jenis_penjualan
-- ----------------------------
INSERT INTO `t_jenis_penjualan` VALUES ('1', '0', '01', 'TUNAI');
INSERT INTO `t_jenis_penjualan` VALUES ('2', '0', '02', 'KREDIT');
INSERT INTO `t_jenis_penjualan` VALUES ('3', '2', '021', 'FLPP');
INSERT INTO `t_jenis_penjualan` VALUES ('4', '2', '022', 'N-FLPP');

-- ----------------------------
-- Table structure for `t_jobtypes`
-- ----------------------------
DROP TABLE IF EXISTS `t_jobtypes`;
CREATE TABLE `t_jobtypes` (
  `id_tipe` char(15) NOT NULL,
  `jenis_pekerjaan` varchar(50) DEFAULT NULL,
  `publish` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_tipe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_jobtypes
-- ----------------------------
INSERT INTO `t_jobtypes` VALUES ('1', 'TIDAK ADA / KOSONG', '1');
INSERT INTO `t_jobtypes` VALUES ('1382672698', 'PNS', '1');
INSERT INTO `t_jobtypes` VALUES ('1389963171', 'TENTARA', '1');
INSERT INTO `t_jobtypes` VALUES ('1389963521', 'POLISI', '1');
INSERT INTO `t_jobtypes` VALUES ('1392694308', 'PETANI', '1');
INSERT INTO `t_jobtypes` VALUES ('1393648693', 'Arsitek', '1');
INSERT INTO `t_jobtypes` VALUES ('1393648917', 'Apoteker', '1');
INSERT INTO `t_jobtypes` VALUES ('1393649124', 'Akuntan', '1');
INSERT INTO `t_jobtypes` VALUES ('1393649176', 'Astronot', '1');
INSERT INTO `t_jobtypes` VALUES ('1393649266', 'Bidan', '1');
INSERT INTO `t_jobtypes` VALUES ('1395159962', 'SWASTA', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205063', 'BURUH', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205080', 'DOKTER', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205088', 'DOSEN', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205099', 'DIREKTUR', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205107', 'DESAINER', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205121', 'GURU', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205130', 'HAKIM', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205142', 'KASIR', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205152', 'MASINIS', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205162', 'NELAYAN', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205184', 'POLITIKUS', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205213', 'PRAMUGARI', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205222', 'PENGUSAHA', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205237', 'PSIKOLOG', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205248', 'PROGRAMMER', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205258', 'SATPAM', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205424', 'WIRASWASTA', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205437', 'SOPIR', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205446', 'NOTARIS', '1');
INSERT INTO `t_jobtypes` VALUES ('1395205459', 'PENELITI', '1');
INSERT INTO `t_jobtypes` VALUES ('1395547889', 'BUMN', '1');
INSERT INTO `t_jobtypes` VALUES ('1395547901', 'BUMD', '1');
INSERT INTO `t_jobtypes` VALUES ('1395547921', 'Instansi', '1');
INSERT INTO `t_jobtypes` VALUES ('1395547931', 'Departement', '1');
INSERT INTO `t_jobtypes` VALUES ('1395547941', 'Pemda', '1');
INSERT INTO `t_jobtypes` VALUES ('1395547955', 'Swasta Asing', '1');
INSERT INTO `t_jobtypes` VALUES ('1395547961', 'PMA', '1');
INSERT INTO `t_jobtypes` VALUES ('1395547986', 'SWASTA BESAR', '1');
INSERT INTO `t_jobtypes` VALUES ('1395547996', 'SWASTA MENENGAH', '1');
INSERT INTO `t_jobtypes` VALUES ('1395548005', 'PMDN', '1');
INSERT INTO `t_jobtypes` VALUES ('1395548019', 'PROFESIONAL', '1');
INSERT INTO `t_jobtypes` VALUES ('1395548033', 'WIRASWASTA BESAR', '1');
INSERT INTO `t_jobtypes` VALUES ('1395548046', 'WIRASWASTA MENENGAH', '1');
INSERT INTO `t_jobtypes` VALUES ('999', 'LAINNYA ....', '1');

-- ----------------------------
-- Table structure for `t_kapling`
-- ----------------------------
DROP TABLE IF EXISTS `t_kapling`;
CREATE TABLE `t_kapling` (
  `id_kapling` char(15) NOT NULL,
  `id_proyek` char(15) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `luas` char(5) DEFAULT NULL,
  `luasreal` char(5) DEFAULT NULL,
  `nib` char(15) DEFAULT NULL,
  `hgb` char(15) DEFAULT NULL,
  `publish` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_kapling`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_kapling
-- ----------------------------
INSERT INTO `t_kapling` VALUES ('13930896711', '1393089671', 'Delia raya I No 1', '276', null, '04793', '371', '1');
INSERT INTO `t_kapling` VALUES ('13930896712', '1393089671', 'Delia raya I No 2', '66', null, '04794', '372', '1');
INSERT INTO `t_kapling` VALUES ('13930896713', '1393089671', 'Delia raya I No 3', '66', null, '04795', '373', '1');
INSERT INTO `t_kapling` VALUES ('13930896714', '1393089671', 'Delia raya I No 4', '69', null, '04796', '374', '1');
INSERT INTO `t_kapling` VALUES ('13930896715', '1393089671', 'Delia raya I No 5', '69', null, '04797', '375', '1');
INSERT INTO `t_kapling` VALUES ('13930896716', '1393089671', 'Delia raya I No 6', '68', null, '04798', '376', '1');
INSERT INTO `t_kapling` VALUES ('13930896717', '1393089671', 'Delia Utara I No 1', '66', null, '04833', '411', '1');
INSERT INTO `t_kapling` VALUES ('13930896718', '1393089671', 'Delia Utara I No 2', '66', null, '04834', '412', '1');
INSERT INTO `t_kapling` VALUES ('13930896719', '1393089671', 'Delia Utara I No 3', '66', null, '04835', '413', '1');
INSERT INTO `t_kapling` VALUES ('13930896720', '1393089671', 'Delia Utara I No 4', '66', null, '04836', '414', '1');
INSERT INTO `t_kapling` VALUES ('13930896721', '1393089671', 'Delia Utara I No 5', '66', null, '04837', '415', '1');
INSERT INTO `t_kapling` VALUES ('13930896722', '1393089671', 'Delia Utara I No 6', '66', null, '04885', '463', '1');
INSERT INTO `t_kapling` VALUES ('13930896723', '1393089671', 'Delia Utara I No 7', '66', null, '04884', '462', '1');
INSERT INTO `t_kapling` VALUES ('13930896724', '1393089671', 'Delia Utara I No 8', '66', null, '04883', '461', '1');
INSERT INTO `t_kapling` VALUES ('13930896725', '1393089671', 'Delia Utara I No 9', '66', null, '04882', '460', '1');
INSERT INTO `t_kapling` VALUES ('13930896726', '1393089671', 'Delia Utara I No 10', '66', null, '04881', '459', '1');
INSERT INTO `t_kapling` VALUES ('13930896727', '1393089671', 'Delia Utara I No 11', '66', null, '04880', '458', '1');
INSERT INTO `t_kapling` VALUES ('13930896728', '1393089671', 'Delia Utara I No 12', '66', null, '04879', '457', '1');
INSERT INTO `t_kapling` VALUES ('13930896729', '1393089671', 'Delia Utara I No 12A', '81', null, '04878', '456', '1');
INSERT INTO `t_kapling` VALUES ('13930896730', '1393089671', 'Delia Utara I No 14', '83', null, '04877', '455', '1');
INSERT INTO `t_kapling` VALUES ('13930896731', '1393089671', 'Delia Utara I No 15', '76', null, '04876', '454', '1');
INSERT INTO `t_kapling` VALUES ('13930896732', '1393089671', 'Delia Utara I No 16', '69', null, '04875', '453', '1');
INSERT INTO `t_kapling` VALUES ('13930896733', '1393089671', 'Delia Utara I No 17', '66', null, '04874', '452', '1');
INSERT INTO `t_kapling` VALUES ('13930896734', '1393089671', 'Delia Utara I No 18', '66', null, '04873', '451', '1');
INSERT INTO `t_kapling` VALUES ('13930896735', '1393089671', 'Delia Utara I No 19', '66', null, '04872', '450', '1');
INSERT INTO `t_kapling` VALUES ('13930896736', '1393089671', 'Delia Utara I No 20', '66', null, '04871', '449', '1');
INSERT INTO `t_kapling` VALUES ('13930896737', '1393089671', 'Delia Utara I No 21', '66', null, '04870', '448', '1');
INSERT INTO `t_kapling` VALUES ('13930896738', '1393089671', 'Delia Utara I No 22', '66', null, '04869', '447', '1');
INSERT INTO `t_kapling` VALUES ('13930896739', '1393089671', 'Delia Utara I No 23', '66', null, '04868', '446', '1');
INSERT INTO `t_kapling` VALUES ('13930896740', '1393089671', 'Delia Utara II No 1', '66', null, '04818', '396', '1');
INSERT INTO `t_kapling` VALUES ('13930896741', '1393089671', 'Delia Utara II No 2', '66', null, '04817', '395', '1');
INSERT INTO `t_kapling` VALUES ('13930896742', '1393089671', 'Delia Utara II No 3', '66', null, '04816', '394', '1');
INSERT INTO `t_kapling` VALUES ('13930896743', '1393089671', 'Delia Utara II No 4', '66', null, '04815', '393', '1');
INSERT INTO `t_kapling` VALUES ('13930896744', '1393089671', 'Delia Utara II No 5', '66', null, '04814', '392', '1');
INSERT INTO `t_kapling` VALUES ('13930896745', '1393089671', 'Delia Utara II No 6', '66', null, '04812', '390', '1');
INSERT INTO `t_kapling` VALUES ('13930896746', '1393089671', 'Delia Utara II No 7', '66', null, '04811', '389', '1');
INSERT INTO `t_kapling` VALUES ('13930896747', '1393089671', 'Delia Utara II No 8', '77', null, '04810', '388', '1');
INSERT INTO `t_kapling` VALUES ('13930896748', '1393089671', 'Delia Utara II No 9', '80', null, '04809', '387', '1');
INSERT INTO `t_kapling` VALUES ('13930896749', '1393089671', 'Delia Utara II No 10', '83', null, '04808', '386', '1');
INSERT INTO `t_kapling` VALUES ('13930896750', '1393089671', 'Delia Utara III No 1', '76', null, '04845', '423', '1');
INSERT INTO `t_kapling` VALUES ('13930896751', '1393089671', 'Delia Utara III No 2', '66', null, '04844', '422', '1');
INSERT INTO `t_kapling` VALUES ('13930896752', '1393089671', 'Delia Utara III No 3', '66', null, '04843', '421', '1');
INSERT INTO `t_kapling` VALUES ('13930896753', '1393089671', 'Delia Utara III No 4', '66', null, '04842', '420', '1');
INSERT INTO `t_kapling` VALUES ('13930896754', '1393089671', 'Delia Utara III No 5', '66', null, '04841', '419', '1');
INSERT INTO `t_kapling` VALUES ('13930896755', '1393089671', 'Delia Utara III No 6', '66', null, '04840', '418', '1');
INSERT INTO `t_kapling` VALUES ('13930896756', '1393089671', 'Delia Utara III No 7', '66', null, '04838', '417', '1');
INSERT INTO `t_kapling` VALUES ('13930896757', '1393089671', 'Delia Utara III No 8', '115', null, '04838', '416', '1');
INSERT INTO `t_kapling` VALUES ('13930896758', '1393089671', 'Delia Utara III No 9', '118', null, '04813', '391', '1');
INSERT INTO `t_kapling` VALUES ('13930896759', '1393089671', 'Delia Utara IV No 1', '72', null, '04846', '424', '1');
INSERT INTO `t_kapling` VALUES ('13930896760', '1393089671', 'Delia Utara IV No 2', '66', null, '04847', '425', '1');
INSERT INTO `t_kapling` VALUES ('13930896761', '1393089671', 'Delia Utara IV No 3', '66', null, '04848', '426', '1');
INSERT INTO `t_kapling` VALUES ('13930896762', '1393089671', 'Delia Utara IV No 4', '66', null, '04849', '427', '1');
INSERT INTO `t_kapling` VALUES ('13930896763', '1393089671', 'Delia Utara IV No 5', '66', null, '04850', '428', '1');
INSERT INTO `t_kapling` VALUES ('13930896764', '1393089671', 'Delia Utara IV No 6', '66', null, '04851', '429', '1');
INSERT INTO `t_kapling` VALUES ('13930896765', '1393089671', 'Delia Utara IV No 7', '120', null, '04852', '430', '1');
INSERT INTO `t_kapling` VALUES ('13930896766', '1393089671', 'Delia Utara IV No 8', '119', null, '04853', '431', '1');
INSERT INTO `t_kapling` VALUES ('13930896767', '1393089671', 'Delia Utara IV No 9', '66', null, '04854', '432', '1');
INSERT INTO `t_kapling` VALUES ('13930896768', '1393089671', 'Delia Utara IV No 10', '66', null, '04855', '433', '1');
INSERT INTO `t_kapling` VALUES ('13930896769', '1393089671', 'Delia Utara IV No 11', '66', null, '04856', '434', '1');
INSERT INTO `t_kapling` VALUES ('13930896770', '1393089671', 'Delia Utara IV No 12', '98', null, '04857', '435', '1');
INSERT INTO `t_kapling` VALUES ('13930896771', '1393089671', 'Delia Utara V No 1', '77', null, '04858', '436', '1');
INSERT INTO `t_kapling` VALUES ('13930896772', '1393089671', 'Delia Utara V No 2', '66', null, '04859', '437', '1');
INSERT INTO `t_kapling` VALUES ('13930896773', '1393089671', 'Delia Utara V No 3', '66', null, '04860', '438', '1');
INSERT INTO `t_kapling` VALUES ('13930896774', '1393089671', 'Delia Utara V No 4', '123', null, '04861', '439', '1');
INSERT INTO `t_kapling` VALUES ('13930896775', '1393089671', 'Delia Utara V No 5', '85', null, '04862', '440', '1');
INSERT INTO `t_kapling` VALUES ('13930896776', '1393089671', 'Delia Utara V No 6', '66', null, '04863', '441', '1');
INSERT INTO `t_kapling` VALUES ('13930896777', '1393089671', 'Delia Utara V No 7', '99', null, '04864', '442', '1');
INSERT INTO `t_kapling` VALUES ('13930896778', '1393089671', 'Delia Utara VI No 1', '77', null, '04865', '443', '1');
INSERT INTO `t_kapling` VALUES ('13930896779', '1393089671', 'Delia Utara VI No 2', '66', null, '04866', '444', '1');
INSERT INTO `t_kapling` VALUES ('13930896780', '1393089671', 'Delia Utara VI No 3', '77', null, '04867', '445', '1');
INSERT INTO `t_kapling` VALUES ('13930896781', '1393089671', 'Delia Selatan I No 1', '99', null, '04799', '377', '1');
INSERT INTO `t_kapling` VALUES ('13930896782', '1393089671', 'Delia Selatan I No 2', '69', null, '04800', '378', '1');
INSERT INTO `t_kapling` VALUES ('13930896783', '1393089671', 'Delia Selatan I No 3', '74', null, '04801', '379', '1');
INSERT INTO `t_kapling` VALUES ('13930896784', '1393089671', 'Delia Selatan I No 4', '78', null, '04802', '380', '1');
INSERT INTO `t_kapling` VALUES ('13930896785', '1393089671', 'Delia Selatan I No 5', '66', null, '04803', '381', '1');
INSERT INTO `t_kapling` VALUES ('13930896786', '1393089671', 'Delia Selatan I No 6', '66', null, '04804', '382', '1');
INSERT INTO `t_kapling` VALUES ('13930896787', '1393089671', 'Delia Selatan I No 7', '66', null, '04805', '383', '1');
INSERT INTO `t_kapling` VALUES ('13930896788', '1393089671', 'Delia Selatan I No 8', '66', null, '04806', '384', '1');
INSERT INTO `t_kapling` VALUES ('13930896789', '1393089671', 'Delia Selatan I No 9', '66', null, '04807', '385', '1');
INSERT INTO `t_kapling` VALUES ('13930896790', '1393089671', 'Delia Selatan I No 10', '66', null, '04819', '397', '1');
INSERT INTO `t_kapling` VALUES ('13930896791', '1393089671', 'Delia Selatan I No 11', '66', null, '04820', '398', '1');
INSERT INTO `t_kapling` VALUES ('13930896792', '1393089671', 'Delia Selatan I No 12', '66', null, '04821', '399', '1');
INSERT INTO `t_kapling` VALUES ('13930896793', '1393089671', 'Delia Selatan I No 12A', '66', null, '04822', '400', '1');
INSERT INTO `t_kapling` VALUES ('13930896794', '1393089671', 'Delia Selatan I No 14', '66', null, '04823', '401', '1');
INSERT INTO `t_kapling` VALUES ('13930896795', '1393089671', 'Delia Selatan I No 15', '66', null, '04824', '402', '1');
INSERT INTO `t_kapling` VALUES ('13930896796', '1393089671', 'Delia Selatan I No 16', '103', null, '04825', '403', '1');
INSERT INTO `t_kapling` VALUES ('13930896797', '1393089671', 'Delia Selatan II No 1', '117', null, '04826', '404', '1');
INSERT INTO `t_kapling` VALUES ('13930896798', '1393089671', 'Delia Selatan II No 2', '66', null, '04827', '405', '1');
INSERT INTO `t_kapling` VALUES ('13930896799', '1393089671', 'Delia Selatan II No 3', '66', null, '04828', '406', '1');
INSERT INTO `t_kapling` VALUES ('13930896800', '1393089671', 'Delia Selatan II No 4', '66', null, '04829', '407', '1');
INSERT INTO `t_kapling` VALUES ('13930896801', '1393089671', 'Delia Selatan II No 5', '66', null, '04830', '408', '1');
INSERT INTO `t_kapling` VALUES ('13930896802', '1393089671', 'Delia Selatan II No 6', '66', null, '04831', '409', '1');
INSERT INTO `t_kapling` VALUES ('13930896803', '1393089671', 'Delia Selatan II No 7', '66', null, '04832', '410', '1');
INSERT INTO `t_kapling` VALUES ('13930896804', '1393089671', 'Delia Selatan II No 8', '66', null, '05016', '594', '1');
INSERT INTO `t_kapling` VALUES ('13930896805', '1393089671', 'Delia Selatan II No 9', '66', null, '05017', '595', '1');
INSERT INTO `t_kapling` VALUES ('13930896806', '1393089671', 'Delia Selatan II No 10', '66', null, '05018', '591', '1');
INSERT INTO `t_kapling` VALUES ('13930896807', '1393089671', 'Delia Selatan II No 11', '66', null, '05019', '590', '1');
INSERT INTO `t_kapling` VALUES ('13930896808', '1393089671', 'Delia Selatan II No 12', '66', null, '05020', '589', '1');
INSERT INTO `t_kapling` VALUES ('13930896809', '1393089671', 'Delia Selatan II No 12A', '66', null, '05021', '587', '1');
INSERT INTO `t_kapling` VALUES ('13930896810', '1393089671', 'Delia Selatan II No 14', '66', null, '05022', '600', '1');
INSERT INTO `t_kapling` VALUES ('13930896811', '1393089671', 'Delia Selatan II No 15', '66', null, '05023', '601', '1');
INSERT INTO `t_kapling` VALUES ('13930896812', '1393089671', 'Delia Selatan III No 1', '66', null, '05008', '586', '1');
INSERT INTO `t_kapling` VALUES ('13930896813', '1393089671', 'Delia Selatan III No 2', '66', null, '05009', '587', '1');
INSERT INTO `t_kapling` VALUES ('13930896814', '1393089671', 'Delia Selatan III No 3', '66', null, '05010', '588', '1');
INSERT INTO `t_kapling` VALUES ('13930896815', '1393089671', 'Delia Selatan III No 4', '66', null, '05011', '589', '1');
INSERT INTO `t_kapling` VALUES ('13930896816', '1393089671', 'Delia Selatan III No 5', '66', null, '05012', '590', '1');
INSERT INTO `t_kapling` VALUES ('13930896817', '1393089671', 'Delia Selatan III No 6', '66', null, '05013', '591', '1');
INSERT INTO `t_kapling` VALUES ('13930896818', '1393089671', 'Delia Selatan III No 7', '66', null, '05014', '592', '1');
INSERT INTO `t_kapling` VALUES ('13930896819', '1393089671', 'Delia Selatan III No 8', '66', null, '05015', '593', '1');
INSERT INTO `t_kapling` VALUES ('13930896820', '1393089671', 'Delia Selatan III No 9', '66', null, '05002', '580', '1');
INSERT INTO `t_kapling` VALUES ('13930896821', '1393089671', 'Delia Selatan III No 10', '66', null, '05003', '581', '1');
INSERT INTO `t_kapling` VALUES ('13930896822', '1393089671', 'Delia Selatan III No 11', '66', null, '05004', '582', '1');
INSERT INTO `t_kapling` VALUES ('13930896823', '1393089671', 'Delia Selatan III No 12', '67', null, '05005', '583', '1');
INSERT INTO `t_kapling` VALUES ('13930896824', '1393089671', 'Delia Selatan III No 12A', '67', null, '05006', '584', '1');
INSERT INTO `t_kapling` VALUES ('13930896825', '1393089671', 'Delia Selatan III No 14', '120', null, '05007', '585', '1');
INSERT INTO `t_kapling` VALUES ('13930896826', '1393089671', 'Delia Selatan IV No 1', '100', null, '04996', '574', '1');
INSERT INTO `t_kapling` VALUES ('13930896827', '1393089671', 'Delia Selatan IV No 2', '74', null, '04997', '575', '1');
INSERT INTO `t_kapling` VALUES ('13930896828', '1393089671', 'Delia Selatan IV No 3', '73', null, '04998', '576', '1');
INSERT INTO `t_kapling` VALUES ('13930896829', '1393089671', 'Delia Selatan IV No 4', '72', null, '04999', '577', '1');
INSERT INTO `t_kapling` VALUES ('13930896830', '1393089671', 'Delia Selatan IV No 5', '72', null, '05000', '578', '1');
INSERT INTO `t_kapling` VALUES ('13930896831', '1393089671', 'Delia Selatan IV No 6', '71', null, '050001', '579', '1');
INSERT INTO `t_kapling` VALUES ('13930896832', '1393089671', 'Delia Selatan IV No 7', '66', null, '04989', '567', '1');
INSERT INTO `t_kapling` VALUES ('13930896833', '1393089671', 'Delia Selatan IV No 8', '66', null, '04990', '568', '1');
INSERT INTO `t_kapling` VALUES ('13930896834', '1393089671', 'Delia Selatan IV No 9', '66', null, '04991', '569', '1');
INSERT INTO `t_kapling` VALUES ('13930896835', '1393089671', 'Delia Selatan IV No 10', '66', null, '04992', '570', '1');
INSERT INTO `t_kapling` VALUES ('13930896836', '1393089671', 'Delia Selatan IV No 11', '66', null, '04993', '571', '1');
INSERT INTO `t_kapling` VALUES ('13930896837', '1393089671', 'Delia Selatan IV No 12', '66', null, '04994', '572', '1');
INSERT INTO `t_kapling` VALUES ('13930896838', '1393089671', 'Delia Selatan IV No 12A', '102', null, '04995', '573', '1');
INSERT INTO `t_kapling` VALUES ('13930896839', '1393089671', 'Delia Selatan V No 1', '129', null, '04983', '561', '1');
INSERT INTO `t_kapling` VALUES ('13930896840', '1393089671', 'Delia Selatan V No 2', '66', null, '04984', '562', '1');
INSERT INTO `t_kapling` VALUES ('13930896841', '1393089671', 'Delia Selatan V No 3', '66', null, '04985', '563', '1');
INSERT INTO `t_kapling` VALUES ('13930896842', '1393089671', 'Delia Selatan V No 4', '66', null, '04989', '564', '1');
INSERT INTO `t_kapling` VALUES ('13930896843', '1393089671', 'Delia Selatan V No 5', '66', null, '04987', '565', '1');
INSERT INTO `t_kapling` VALUES ('13930896844', '1393089671', 'Delia Selatan V No 6', '66', null, '04988', '566', '1');
INSERT INTO `t_kapling` VALUES ('13930896845', '1393089671', 'Delia Selatan V No 7', '66', null, '04977', '555', '1');
INSERT INTO `t_kapling` VALUES ('13930896846', '1393089671', 'Delia Selatan V No 8', '66', null, '04978', '556', '1');
INSERT INTO `t_kapling` VALUES ('13930896847', '1393089671', 'Delia Selatan V No 9', '66', null, '04979', '557', '1');
INSERT INTO `t_kapling` VALUES ('13930896848', '1393089671', 'Delia Selatan V No 10', '66', null, '04980', '558', '1');
INSERT INTO `t_kapling` VALUES ('13930896849', '1393089671', 'Delia Selatan V No 11', '66', null, '04981', '559', '1');
INSERT INTO `t_kapling` VALUES ('13930896850', '1393089671', 'Delia Selatan V No 12', '78', null, '04982', '560', '1');
INSERT INTO `t_kapling` VALUES ('13930896851', '1393089671', 'Delia Selatan VI No 1', '108', null, '04972', '550', '1');
INSERT INTO `t_kapling` VALUES ('13930896852', '1393089671', 'Delia Selatan VI No 2', '66', null, '04973', '551', '1');
INSERT INTO `t_kapling` VALUES ('13930896853', '1393089671', 'Delia Selatan VI No 3', '66', null, '04974', '552', '1');
INSERT INTO `t_kapling` VALUES ('13930896854', '1393089671', 'Delia Selatan VI No 4', '66', null, '04975', '553', '1');
INSERT INTO `t_kapling` VALUES ('13930896855', '1393089671', 'Delia Selatan VI No 5', '66', null, '04976', '554', '1');
INSERT INTO `t_kapling` VALUES ('13930896856', '1393089671', 'Delia Selatan VI No 6', '66', null, '04967', '545', '1');
INSERT INTO `t_kapling` VALUES ('13930896857', '1393089671', 'Delia Selatan VI No 7', '66', null, '04968', '546', '1');
INSERT INTO `t_kapling` VALUES ('13930896858', '1393089671', 'Delia Selatan VI No 8', '66', null, '04969', '547', '1');
INSERT INTO `t_kapling` VALUES ('13930896859', '1393089671', 'Delia Selatan VI No 9', '69', null, '04970', '548', '1');
INSERT INTO `t_kapling` VALUES ('13930896860', '1393089671', 'Delia Selatan VI No 10', '78', null, '04971', '549', '1');
INSERT INTO `t_kapling` VALUES ('13930896861', '1393089671', 'Delia Timur I No 1', '116', null, '04963', '550', '1');
INSERT INTO `t_kapling` VALUES ('13930896862', '1393089671', 'Delia Timur I No 2', '66', null, '04964', '542', '1');
INSERT INTO `t_kapling` VALUES ('13930896863', '1393089671', 'Delia Timur I No 3', '66', null, '04965', '543', '1');
INSERT INTO `t_kapling` VALUES ('13930896864', '1393089671', 'Delia Timur I No 4', '66', null, '04966', '544', '1');
INSERT INTO `t_kapling` VALUES ('13930896865', '1393089671', 'Delia Timur I No 5', '66', null, '04942', '520', '1');
INSERT INTO `t_kapling` VALUES ('13930896866', '1393089671', 'Delia Timur I No 6', '66', null, '04943', '521', '1');
INSERT INTO `t_kapling` VALUES ('13930896867', '1393089671', 'Delia Timur I No 7', '66', null, '04944', '522', '1');
INSERT INTO `t_kapling` VALUES ('13930896868', '1393089671', 'Delia Timur I No 8', '66', null, '04945', '523', '1');
INSERT INTO `t_kapling` VALUES ('13930896869', '1393089671', 'Delia Timur I No 9', '66', null, '04946', '524', '1');
INSERT INTO `t_kapling` VALUES ('13930896870', '1393089671', 'Delia Timur I No 10', '66', null, '04947', '525', '1');
INSERT INTO `t_kapling` VALUES ('13930896871', '1393089671', 'Delia Timur I No 11', '66', null, '04948', '526', '1');
INSERT INTO `t_kapling` VALUES ('13930896872', '1393089671', 'Delia Timur I No 12', '107', null, '04949', '527', '1');
INSERT INTO `t_kapling` VALUES ('13930896873', '1393089671', 'Delia Timur I No 12A', '142', null, '04950', '528', '1');
INSERT INTO `t_kapling` VALUES ('13930896874', '1393089671', 'Delia Timur I No 14', '87', null, '04951', '529', '1');
INSERT INTO `t_kapling` VALUES ('13930896875', '1393089671', 'Delia Timur I No 15', '86', null, '04952', '530', '1');
INSERT INTO `t_kapling` VALUES ('13930896876', '1393089671', 'Delia Timur I No 16', '82', null, '04953', '531', '1');
INSERT INTO `t_kapling` VALUES ('13930896877', '1393089671', 'Delia Timur I No 17', '79', null, '04954', '532', '1');
INSERT INTO `t_kapling` VALUES ('13930896878', '1393089671', 'Delia Timur I No 18', '74', null, '04955', '533', '1');
INSERT INTO `t_kapling` VALUES ('13930896879', '1393089671', 'Delia Timur I No 19', '70', null, '04956', '534', '1');
INSERT INTO `t_kapling` VALUES ('13930896880', '1393089671', 'Delia Timur I No 20', '73', null, '04957', '535', '1');
INSERT INTO `t_kapling` VALUES ('13930896881', '1393089671', 'Delia Timur I No 21', '75', null, '04958', '536', '1');
INSERT INTO `t_kapling` VALUES ('13930896882', '1393089671', 'Delia Timur I No 22', '75', null, '04959', '537', '1');
INSERT INTO `t_kapling` VALUES ('13930896883', '1393089671', 'Delia Timur I No 23', '76', null, '04960', '538', '1');
INSERT INTO `t_kapling` VALUES ('13930896884', '1393089671', 'Delia Timur I No 24', '77', null, '04961', '539', '1');
INSERT INTO `t_kapling` VALUES ('13930896885', '1393089671', 'Delia Timur I No 25', '83', null, '04962', '540', '1');
INSERT INTO `t_kapling` VALUES ('13930896886', '1393089671', 'Delia Timur II No 1', '66', null, '04941', '519', '1');
INSERT INTO `t_kapling` VALUES ('13930896887', '1393089671', 'Delia Timur II No 2', '66', null, '04940', '518', '1');
INSERT INTO `t_kapling` VALUES ('13930896888', '1393089671', 'Delia Timur II No 3', '66', null, '04939', '517', '1');
INSERT INTO `t_kapling` VALUES ('13930896889', '1393089671', 'Delia Timur II No 4', '66', null, '04938', '516', '1');
INSERT INTO `t_kapling` VALUES ('13930896890', '1393089671', 'Delia Timur II No 5', '66', null, '04937', '515', '1');
INSERT INTO `t_kapling` VALUES ('13930896891', '1393089671', 'Delia Timur II No 6', '67', null, '04936', '514', '1');
INSERT INTO `t_kapling` VALUES ('13930896892', '1393089671', 'Delia Timur II No 7', '66', null, '04935', '513', '1');
INSERT INTO `t_kapling` VALUES ('13930896893', '1393089671', 'Delia Timur II No 8', '107', null, '04934', '512', '1');
INSERT INTO `t_kapling` VALUES ('13930896894', '1393089671', 'Delia Timur II No 9', '102', null, '04933', '511', '1');
INSERT INTO `t_kapling` VALUES ('13930896895', '1393089671', 'Delia Timur II No 10', '67', null, '04932', '510', '1');
INSERT INTO `t_kapling` VALUES ('13930896896', '1393089671', 'Delia Timur II No 11', '67', null, '04931', '509', '1');
INSERT INTO `t_kapling` VALUES ('13930896897', '1393089671', 'Delia Timur II No 12', '67', null, '04930', '508', '1');
INSERT INTO `t_kapling` VALUES ('13930896898', '1393089671', 'Delia Timur II No 12A', '68', null, '04929', '507', '1');
INSERT INTO `t_kapling` VALUES ('13930896899', '1393089671', 'Delia Timur II No 14', '67', null, '04928', '506', '1');
INSERT INTO `t_kapling` VALUES ('13930896900', '1393089671', 'Delia Timur II No 15', '68', null, '04927', '505', '1');
INSERT INTO `t_kapling` VALUES ('13930896901', '1393089671', 'Delia Timur II No 16', '67', null, '04926', '504', '1');
INSERT INTO `t_kapling` VALUES ('13930896902', '1393089671', 'Delia Timur III No 1', '66', null, '04925', '503', '1');
INSERT INTO `t_kapling` VALUES ('13930896903', '1393089671', 'Delia Timur III No 2', '66', null, '04924', '502', '1');
INSERT INTO `t_kapling` VALUES ('13930896904', '1393089671', 'Delia Timur III No 3', '66', null, '04923', '501', '1');
INSERT INTO `t_kapling` VALUES ('13930896905', '1393089671', 'Delia Timur III No 4', '66', null, '04922', '500', '1');
INSERT INTO `t_kapling` VALUES ('13930896906', '1393089671', 'Delia Timur III No 5', '66', null, '04921', '499', '1');
INSERT INTO `t_kapling` VALUES ('13930896907', '1393089671', 'Delia Timur III No 6', '66', null, '04920', '498', '1');
INSERT INTO `t_kapling` VALUES ('13930896908', '1393089671', 'Delia Timur III No 7', '66', null, '04919', '497', '1');
INSERT INTO `t_kapling` VALUES ('13930896909', '1393089671', 'Delia Timur III No 8', '101', null, '04918', '496', '1');
INSERT INTO `t_kapling` VALUES ('13930896910', '1393089671', 'Delia Timur III No 9', '66', null, '04917', '495', '1');
INSERT INTO `t_kapling` VALUES ('13930896911', '1393089671', 'Delia Timur III No 10', '66', null, '04916', '494', '1');
INSERT INTO `t_kapling` VALUES ('13930896912', '1393089671', 'Delia Timur III No 11', '66', null, '04915', '493', '1');
INSERT INTO `t_kapling` VALUES ('13930896913', '1393089671', 'Delia Timur III No 12', '66', null, '04914', '492', '1');
INSERT INTO `t_kapling` VALUES ('13930896914', '1393089671', 'Delia Timur III No 12A', '66', null, '04913', '491', '1');
INSERT INTO `t_kapling` VALUES ('13930896915', '1393089671', 'Delia Timur III No 14', '66', null, '04912', '490', '1');
INSERT INTO `t_kapling` VALUES ('13930896916', '1393089671', 'Delia Timur IV No 1', '66', null, '04911', '489', '1');
INSERT INTO `t_kapling` VALUES ('13930896917', '1393089671', 'Delia Timur IV No 2', '66', null, '04910', '488', '1');
INSERT INTO `t_kapling` VALUES ('13930896918', '1393089671', 'Delia Timur IV No 3', '66', null, '04909', '487', '1');
INSERT INTO `t_kapling` VALUES ('13930896919', '1393089671', 'Delia Timur IV No 4', '66', null, '04908', '486', '1');
INSERT INTO `t_kapling` VALUES ('13930896920', '1393089671', 'Delia Timur IV No 5', '66', null, '04907', '485', '1');
INSERT INTO `t_kapling` VALUES ('13930896921', '1393089671', 'Delia Timur IV No 6', '66', null, '04906', '484', '1');
INSERT INTO `t_kapling` VALUES ('13930896922', '1393089671', 'Delia Timur IV No 7', '66', null, '04905', '483', '1');
INSERT INTO `t_kapling` VALUES ('13930896923', '1393089671', 'Delia Timur IV No 8', '108', null, '04904', '482', '1');
INSERT INTO `t_kapling` VALUES ('13930896924', '1393089671', 'Delia Timur IV No 9', '94', null, '04903', '481', '1');
INSERT INTO `t_kapling` VALUES ('13930896925', '1393089671', 'Delia Timur IV No 10', '66', null, '04902', '480', '1');
INSERT INTO `t_kapling` VALUES ('13930896926', '1393089671', 'Delia Timur IV No 11', '66', null, '04901', '479', '1');
INSERT INTO `t_kapling` VALUES ('13930896927', '1393089671', 'Delia Timur IV No 12', '66', null, '04900', '478', '1');
INSERT INTO `t_kapling` VALUES ('13930896928', '1393089671', 'Delia Timur IV No 12A', '66', null, '04899', '477', '1');
INSERT INTO `t_kapling` VALUES ('13930896929', '1393089671', 'Delia Timur  V No 1', '66', null, '04898', '476', '1');
INSERT INTO `t_kapling` VALUES ('13930896930', '1393089671', 'Delia Timur  V No 2', '66', null, '04897', '475', '1');
INSERT INTO `t_kapling` VALUES ('13930896931', '1393089671', 'Delia Timur  V No 3', '66', null, '04896', '474', '1');
INSERT INTO `t_kapling` VALUES ('13930896932', '1393089671', 'Delia Timur  V No 4', '66', null, '04895', '473', '1');
INSERT INTO `t_kapling` VALUES ('13930896933', '1393089671', 'Delia Timur  V No 5', '95', null, '04894', '472', '1');
INSERT INTO `t_kapling` VALUES ('13930896934', '1393089671', 'Delia Timur  V No 6', '93', null, '04893', '471', '1');
INSERT INTO `t_kapling` VALUES ('13930896935', '1393089671', 'Delia Timur  V No 7', '66', null, '04892', '470', '1');
INSERT INTO `t_kapling` VALUES ('13930896936', '1393089671', 'Delia Timur  V No 8', '66', null, '04891', '469', '1');
INSERT INTO `t_kapling` VALUES ('13930896937', '1393089671', 'Delia Timur  V No 9', '66', null, '04890', '468', '1');
INSERT INTO `t_kapling` VALUES ('13930896938', '1393089671', 'Delia Timur  V No 10', '66', null, '04889', '467', '1');
INSERT INTO `t_kapling` VALUES ('13930896939', '1393089671', 'Delia Timur  V No 11', '66', null, '04888', '466', '1');
INSERT INTO `t_kapling` VALUES ('13930896940', '1393089671', 'Delia Timur  V No 12', '66', null, '0887', '465', '1');
INSERT INTO `t_kapling` VALUES ('13930896941', '1393089671', 'Delia Timur  V No 12A', '67', null, '04886', '464', '1');
INSERT INTO `t_kapling` VALUES ('13930905221', '1393090522', 'Toko A1', '50', null, '00319', '27', '1');
INSERT INTO `t_kapling` VALUES ('13930905222', '1393090522', 'Toko A2', '50', null, '00320', '28', '1');
INSERT INTO `t_kapling` VALUES ('13930905223', '1393090522', 'Toko A3', '50', null, '00321', '29', '1');
INSERT INTO `t_kapling` VALUES ('13930905224', '1393090522', 'Toko B1', '50', null, '00322', '30', '1');
INSERT INTO `t_kapling` VALUES ('13930905225', '1393090522', 'Toko B2', '50', null, '00323', '31', '1');
INSERT INTO `t_kapling` VALUES ('13930905226', '1393090522', 'Toko B3', '50', null, '00324', '32', '1');
INSERT INTO `t_kapling` VALUES ('13930905227', '1393090522', 'Griya Prima Raya No 1', '104', null, '00375', '83', '1');
INSERT INTO `t_kapling` VALUES ('13930905228', '1393090522', 'Griya Prima Raya No 2', '101', null, '00376', '84', '1');
INSERT INTO `t_kapling` VALUES ('13930905229', '1393090522', 'Griya Prima Raya No 3', '108', null, '00377', '85', '1');
INSERT INTO `t_kapling` VALUES ('13930905230', '1393090522', 'Griya Prima Raya No 4', '114', null, '00378', '86', '1');
INSERT INTO `t_kapling` VALUES ('13930905231', '1393090522', 'Griya Prima Raya No 5', '121', null, '00379', '87', '1');
INSERT INTO `t_kapling` VALUES ('13930905232', '1393090522', 'Griya Prima Raya No 6', '126', null, '00380', '88', '1');
INSERT INTO `t_kapling` VALUES ('13930905233', '1393090522', 'Griya Prima Raya No 7', '129', null, '00381', '89', '1');
INSERT INTO `t_kapling` VALUES ('13930905234', '1393090522', 'Griya Prima Raya No 8', '131', null, '00382', '90', '1');
INSERT INTO `t_kapling` VALUES ('13930905235', '1393090522', 'Griya Prima Raya No 9', '134', null, '00383', '91', '1');
INSERT INTO `t_kapling` VALUES ('13930905236', '1393090522', 'Griya Prima Raya No 10', '137', null, '00384', '92', '1');
INSERT INTO `t_kapling` VALUES ('13930905237', '1393090522', 'Griya Prima Raya No 11', '137', null, '00385', '93', '1');
INSERT INTO `t_kapling` VALUES ('13930905238', '1393090522', 'Griya Prima Raya No 12', '129', null, '00386', '94', '1');
INSERT INTO `t_kapling` VALUES ('13930905239', '1393090522', 'Griya Prima Raya No 13', '116', null, '00387', '95', '1');
INSERT INTO `t_kapling` VALUES ('13930905240', '1393090522', 'Griya Prima Raya No 14', '133', null, '00388', '96', '1');
INSERT INTO `t_kapling` VALUES ('13930905241', '1393090522', 'Griya Prima Utara Raya No 1', '99', null, '00325', '33', '1');
INSERT INTO `t_kapling` VALUES ('13930905242', '1393090522', 'Griya Prima Utara Raya No 2', '67', null, '00326', '34', '1');
INSERT INTO `t_kapling` VALUES ('13930905243', '1393090522', 'Griya Prima Utara Raya No 3', '67', null, '00327', '35', '1');
INSERT INTO `t_kapling` VALUES ('13930905244', '1393090522', 'Griya Prima Utara Raya No 4', '67', null, '00328', '36', '1');
INSERT INTO `t_kapling` VALUES ('13930905245', '1393090522', 'Griya Prima Utara Raya No 5', '67', null, '00329', '37', '1');
INSERT INTO `t_kapling` VALUES ('13930905246', '1393090522', 'Griya Prima Utara Raya No 6', '68', null, '00330', '38', '1');
INSERT INTO `t_kapling` VALUES ('13930905247', '1393090522', 'Griya Prima Utara Raya No 7', '68', null, '00331', '39', '1');
INSERT INTO `t_kapling` VALUES ('13930905248', '1393090522', 'Griya Prima Utara Raya No 8', '68', null, '00332', '40', '1');
INSERT INTO `t_kapling` VALUES ('13930905249', '1393090522', 'Griya Prima Utara Raya No 9', '68', null, '00333', '41', '1');
INSERT INTO `t_kapling` VALUES ('13930905250', '1393090522', 'Griya Prima Utara Raya No 10', '68', null, '00334', '42', '1');
INSERT INTO `t_kapling` VALUES ('13930905251', '1393090522', 'Griya Prima Utara Raya No 11', '69', null, '00335', '43', '1');
INSERT INTO `t_kapling` VALUES ('13930905252', '1393090522', 'Griya Prima Utara Raya No 12', '69', null, '00336', '44', '1');
INSERT INTO `t_kapling` VALUES ('13930905253', '1393090522', 'Griya Prima Utara Raya No 13', '69', null, '00337', '45', '1');
INSERT INTO `t_kapling` VALUES ('13930905254', '1393090522', 'Griya Prima Utara Raya No 14', '69', null, '00338', '46', '1');
INSERT INTO `t_kapling` VALUES ('13930905255', '1393090522', 'Griya Prima Utara Raya No 15', '69', null, '00339', '47', '1');
INSERT INTO `t_kapling` VALUES ('13930905256', '1393090522', 'Griya Prima Utara Raya No 16', '71', null, '00340', '48', '1');
INSERT INTO `t_kapling` VALUES ('13930905257', '1393090522', 'Griya Prima Utara Raya No 17', '70', null, '00341', '49', '1');
INSERT INTO `t_kapling` VALUES ('13930905258', '1393090522', 'Griya Prima Utara Raya No 18', '70', null, '00342', '50', '1');
INSERT INTO `t_kapling` VALUES ('13930905259', '1393090522', 'Griya Prima Utara Raya No 19', '70', null, '00343', '51', '1');
INSERT INTO `t_kapling` VALUES ('13930905260', '1393090522', 'Griya Prima Utara Raya No 20', '70', null, '00344', '52', '1');
INSERT INTO `t_kapling` VALUES ('13930905261', '1393090522', 'Griya Prima Utara Raya No 21', '71', null, '00345', '53', '1');
INSERT INTO `t_kapling` VALUES ('13930905262', '1393090522', 'Griya Prima Utara Raya No 22', '71', null, '00346', '54', '1');
INSERT INTO `t_kapling` VALUES ('13930905263', '1393090522', 'Griya Prima Utara Raya No 23', '71', null, '00347', '55', '1');
INSERT INTO `t_kapling` VALUES ('13930905264', '1393090522', 'Griya Prima Utara Raya No 24', '71', null, '00348', '56', '1');
INSERT INTO `t_kapling` VALUES ('13930905265', '1393090522', 'Griya Prima Utara Raya No 25', '109', null, '00349', '57', '1');
INSERT INTO `t_kapling` VALUES ('13930905266', '1393090522', 'Griya Prima Utara Raya No 26', '83', null, '00350', '58', '1');
INSERT INTO `t_kapling` VALUES ('13930905267', '1393090522', 'Griya Prima Utara Raya No 27', '66', null, '00351', '59', '1');
INSERT INTO `t_kapling` VALUES ('13930905268', '1393090522', 'Griya Prima Utara Raya No 28', '66', null, '00352', '60', '1');
INSERT INTO `t_kapling` VALUES ('13930905269', '1393090522', 'Griya Prima Utara Raya No 29', '66', null, '00353', '61', '1');
INSERT INTO `t_kapling` VALUES ('13930905270', '1393090522', 'Griya Prima Utara Raya No 30', '66', null, '00354', '62', '1');
INSERT INTO `t_kapling` VALUES ('13930905271', '1393090522', 'Griya Prima Utara Raya No 31', '66', null, '00355', '63', '1');
INSERT INTO `t_kapling` VALUES ('13930905272', '1393090522', 'Griya Prima Utara Raya No 32', '66', null, '00356', '64', '1');
INSERT INTO `t_kapling` VALUES ('13930905273', '1393090522', 'Griya Prima Utara Raya No 33', '66', null, '00357', '65', '1');
INSERT INTO `t_kapling` VALUES ('13930905274', '1393090522', 'Griya Prima Utara Raya No 34', '66', null, '00358', '66', '1');
INSERT INTO `t_kapling` VALUES ('13930905275', '1393090522', 'Griya Prima Utara Raya No 35', '66', null, '00359', '67', '1');
INSERT INTO `t_kapling` VALUES ('13930905276', '1393090522', 'Griya Prima Utara Raya No 36', '66', null, '00360', '68', '1');
INSERT INTO `t_kapling` VALUES ('13930905277', '1393090522', 'Griya Prima Utara Raya No 37', '66', null, '00361', '69', '1');
INSERT INTO `t_kapling` VALUES ('13930905278', '1393090522', 'Griya Prima Utara Raya No 38', '66', null, '00362', '70', '1');
INSERT INTO `t_kapling` VALUES ('13930905279', '1393090522', 'Griya Prima Utara Raya No 39', '66', null, '00363', '71', '1');
INSERT INTO `t_kapling` VALUES ('13930905280', '1393090522', 'Griya Prima Utara Raya No 40', '66', null, '00364', '72', '1');
INSERT INTO `t_kapling` VALUES ('13930905281', '1393090522', 'Griya Prima Utara Raya No 41', '66', null, '00365', '73', '1');
INSERT INTO `t_kapling` VALUES ('13930905282', '1393090522', 'Griya Prima Utara Raya No 42', '66', null, '00366', '74', '1');
INSERT INTO `t_kapling` VALUES ('13930905283', '1393090522', 'Griya Prima Utara Raya No 43', '66', null, '00367', '75', '1');
INSERT INTO `t_kapling` VALUES ('13930905284', '1393090522', 'Griya Prima Utara Raya No 44', '66', null, '00368', '76', '1');
INSERT INTO `t_kapling` VALUES ('13930905285', '1393090522', 'Griya Prima Utara Raya No 45', '66', null, '00369', '77', '1');
INSERT INTO `t_kapling` VALUES ('13930905286', '1393090522', 'Griya Prima Utara Raya No 46', '66', null, '00370', '78', '1');
INSERT INTO `t_kapling` VALUES ('13930905287', '1393090522', 'Griya Prima Utara Raya No 47', '66', null, '00371', '79', '1');
INSERT INTO `t_kapling` VALUES ('13930905288', '1393090522', 'Griya Prima Utara Raya No 48', '66', null, '00372', '80', '1');
INSERT INTO `t_kapling` VALUES ('13930905289', '1393090522', 'Griya Prima Utara Raya No 49', '66', null, '00373', '81', '1');
INSERT INTO `t_kapling` VALUES ('13930905290', '1393090522', 'Griya Prima Utara Raya No 50', '81', null, '00374', '82', '1');
INSERT INTO `t_kapling` VALUES ('13930905291', '1393090522', 'Griya Prima Utara I No 1', '90', null, '00389', '97', '1');
INSERT INTO `t_kapling` VALUES ('13930905292', '1393090522', 'Griya Prima Utara I No 2', '66', null, '00390', '98', '1');
INSERT INTO `t_kapling` VALUES ('13930905293', '1393090522', 'Griya Prima Utara I No 3', '66', null, '00391', '99', '1');
INSERT INTO `t_kapling` VALUES ('13930905294', '1393090522', 'Griya Prima Utara I No 4', '66', null, '00392', '100', '1');
INSERT INTO `t_kapling` VALUES ('13930905295', '1393090522', 'Griya Prima Utara I No 5', '66', null, '00393', '101', '1');
INSERT INTO `t_kapling` VALUES ('13930905296', '1393090522', 'Griya Prima Utara I No 6', '66', null, '00394', '102', '1');
INSERT INTO `t_kapling` VALUES ('13930905297', '1393090522', 'Griya Prima Utara I No 7', '74', null, '00395', '103', '1');
INSERT INTO `t_kapling` VALUES ('13930905298', '1393090522', 'Griya Prima Utara II No 1', '108', null, '00396', '104', '1');
INSERT INTO `t_kapling` VALUES ('13930905299', '1393090522', 'Griya Prima Utara II No 2', '66', null, '00397', '105', '1');
INSERT INTO `t_kapling` VALUES ('13930905300', '1393090522', 'Griya Prima Utara II No 3', '66', null, '00398', '106', '1');
INSERT INTO `t_kapling` VALUES ('13930905301', '1393090522', 'Griya Prima Utara II No 4', '66', null, '00399', '107', '1');
INSERT INTO `t_kapling` VALUES ('13930905302', '1393090522', 'Griya Prima Utara II No 5', '66', null, '00400', '108', '1');
INSERT INTO `t_kapling` VALUES ('13930905303', '1393090522', 'Griya Prima Utara II No 6', '85', null, '00401', '109', '1');
INSERT INTO `t_kapling` VALUES ('13930905304', '1393090522', 'Griya Prima Utara II No 7', '107', null, '00402', '110', '1');
INSERT INTO `t_kapling` VALUES ('13930905305', '1393090522', 'Griya Prima Utara II No 8', '66', null, '00403', '111', '1');
INSERT INTO `t_kapling` VALUES ('13930905306', '1393090522', 'Griya Prima Utara II No 9', '66', null, '00404', '112', '1');
INSERT INTO `t_kapling` VALUES ('13930905307', '1393090522', 'Griya Prima Utara II No 10', '66', null, '00405', '113', '1');
INSERT INTO `t_kapling` VALUES ('13930905308', '1393090522', 'Griya Prima Utara II No 11', '87', null, '00406', '114', '1');
INSERT INTO `t_kapling` VALUES ('13930905309', '1393090522', 'Griya Prima Utara III No 1', '119', null, '00407', '115', '1');
INSERT INTO `t_kapling` VALUES ('13930905310', '1393090522', 'Griya Prima Utara III No 2', '66', null, '00408', '116', '1');
INSERT INTO `t_kapling` VALUES ('13930905311', '1393090522', 'Griya Prima Utara III No 3', '66', null, '00409', '117', '1');
INSERT INTO `t_kapling` VALUES ('13930905312', '1393090522', 'Griya Prima Utara III No 4', '87', null, '00410', '118', '1');
INSERT INTO `t_kapling` VALUES ('13930905313', '1393090522', 'Griya Prima Utara III No 5', '107', null, '00411', '119', '1');
INSERT INTO `t_kapling` VALUES ('13930905314', '1393090522', 'Griya Prima Utara III No 6', '67', null, '00412', '120', '1');
INSERT INTO `t_kapling` VALUES ('13930905315', '1393090522', 'Griya Prima Utara III No 7', '68', null, '00413', '121', '1');
INSERT INTO `t_kapling` VALUES ('13930905316', '1393090522', 'Griya Prima Utara IV No 1', '102', null, '00414', '122', '1');
INSERT INTO `t_kapling` VALUES ('13930905317', '1393090522', 'Griya Prima Utara IV No 2', '70', null, '00415', '123', '1');
INSERT INTO `t_kapling` VALUES ('13930905318', '1393090522', 'Griya Prima Barat I No 1', '82', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905319', '1393090522', 'Griya Prima Barat I No 2', '116', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905320', '1393090522', 'Griya Prima Barat I No 3', '72', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905321', '1393090522', 'Griya Prima Barat I No 4', '67', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905322', '1393090522', 'Griya Prima Barat I No 5', '67', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905323', '1393090522', 'Griya Prima Barat I No 6', '104', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905324', '1393090522', 'Griya Prima Barat II No 1', '71', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905325', '1393090522', 'Griya Prima Barat II No 2', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905326', '1393090522', 'Griya Prima Barat II No 3', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905327', '1393090522', 'Griya Prima Barat II No 4', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905328', '1393090522', 'Griya Prima Barat II No 5', '104', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905329', '1393090522', 'Griya Prima Barat II No 6', '99', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905330', '1393090522', 'Griya Prima Barat II No 7', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905331', '1393090522', 'Griya Prima Barat II No 8', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905332', '1393090522', 'Griya Prima Barat II No 9', '67', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905333', '1393090522', 'Griya Prima Barat II No 10', '67', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905334', '1393090522', 'Griya Prima Barat II No 11', '105', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905335', '1393090522', 'Griya Prima Barat III No 1', '83', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905336', '1393090522', 'Griya Prima Barat III No 2', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905337', '1393090522', 'Griya Prima Barat III No 3', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905338', '1393090522', 'Griya Prima Barat III No 4', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905339', '1393090522', 'Griya Prima Barat III No 5', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905340', '1393090522', 'Griya Prima Barat III No 6', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905341', '1393090522', 'Griya Prima Barat III No 7', '100', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905342', '1393090522', 'Griya Prima Barat III No 8', '85', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905343', '1393090522', 'Griya Prima Barat III No 9', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905344', '1393090522', 'Griya Prima Barat III No 10', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905345', '1393090522', 'Griya Prima Barat III No 11', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905346', '1393090522', 'Griya Prima Barat III No 12', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905347', '1393090522', 'Griya Prima Barat III No 13', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905348', '1393090522', 'Griya Prima Barat III No 14', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905349', '1393090522', 'Griya Prima Barat III No 15', '98', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905350', '1393090522', 'Griya Prima Barat IV  No 1', '69', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905351', '1393090522', 'Griya Prima Barat IV  No 2', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905352', '1393090522', 'Griya Prima Barat IV  No 3', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905353', '1393090522', 'Griya Prima Barat IV  No 4', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905354', '1393090522', 'Griya Prima Barat IV  No 5', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905355', '1393090522', 'Griya Prima Barat IV  No 6', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905356', '1393090522', 'Griya Prima Barat IV  No 7', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905357', '1393090522', 'Griya Prima Barat IV  No 8', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905358', '1393090522', 'Griya Prima Barat IV  No 9', '89', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905359', '1393090522', 'Griya Prima Barat IV  No 10', '71', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905360', '1393090522', 'Griya Prima Barat IV  No 11', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905361', '1393090522', 'Griya Prima Barat IV  No 12', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905362', '1393090522', 'Griya Prima Barat IV  No 13', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905363', '1393090522', 'Griya Prima Barat IV  No 14', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905364', '1393090522', 'Griya Prima Barat IV  No 15', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905365', '1393090522', 'Griya Prima Barat IV  No 16', '95', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905366', '1393090522', 'Griya Prima Barat IV  No 17', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905367', '1393090522', 'Griya Prima Barat IV  No 18', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905368', '1393090522', 'Griya Prima Barat IV  No 19', '76', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905369', '1393090522', 'Griya Prima Barat V  No 1', '71', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905370', '1393090522', 'Griya Prima Barat V  No 2', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905371', '1393090522', 'Griya Prima Barat V  No 3', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905372', '1393090522', 'Griya Prima Barat V  No 4', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905373', '1393090522', 'Griya Prima Barat V  No 5', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905374', '1393090522', 'Griya Prima Barat V  No 6', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905375', '1393090522', 'Griya Prima Barat V  No 7', '84', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905376', '1393090522', 'Griya Prima Barat V  No 8', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905377', '1393090522', 'Griya Prima Barat V  No 9', '66', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905378', '1393090522', 'Griya Prima Barat V  No 10', '97', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905379', '1393090522', 'Griya Prima Barat Raya No 1', '67', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905380', '1393090522', 'Griya Prima Barat Raya No 2', '68', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905381', '1393090522', 'Griya Prima Barat Raya No 3', '133', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905382', '1393090522', 'Griya Prima Barat Raya No 4', '69', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905383', '1393090522', 'Griya Prima Barat Raya No 5', '69', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905384', '1393090522', 'Griya Prima Barat Raya No 6', '70', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905385', '1393090522', 'Griya Prima Barat Raya No 7', '71', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905386', '1393090522', 'Griya Prima Barat Raya No 8', '71', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905387', '1393090522', 'Griya Prima Barat Raya No 9', '72', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905388', '1393090522', 'Griya Prima Barat Raya No 10', '73', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905389', '1393090522', 'Griya Prima Barat Raya No 11', '74', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905390', '1393090522', 'Griya Prima Barat Raya No 12', '74', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905391', '1393090522', 'Griya Prima Barat Raya No 13', '75', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905392', '1393090522', 'Griya Prima Barat Raya No 14', '76', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905393', '1393090522', 'Griya Prima Barat Raya No 15', '76', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905394', '1393090522', 'Griya Prima Barat Raya No 16', '77', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905395', '1393090522', 'Griya Prima Barat Raya No 17', '78', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905396', '1393090522', 'Griya Prima Barat Raya No 18', '78', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905397', '1393090522', 'Griya Prima Barat Raya No 19', '79', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905398', '1393090522', 'Griya Prima Barat Raya No 20', '80', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905399', '1393090522', 'Griya Prima Barat Raya No 21', '80', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905400', '1393090522', 'Griya Prima Barat Raya No 22', '81', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905401', '1393090522', 'Griya Prima Barat Raya No 23', '81', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905402', '1393090522', 'Griya Prima Barat Raya No 24', '82', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905403', '1393090522', 'Griya Prima Barat Raya No 25', '83', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905404', '1393090522', 'Griya Prima Barat Raya No 26', '83', null, null, null, '1');
INSERT INTO `t_kapling` VALUES ('13930905405', '1393090522', 'Griya Prima Barat Raya No 27', '110', null, null, null, '1');

-- ----------------------------
-- Table structure for `t_kas`
-- ----------------------------
DROP TABLE IF EXISTS `t_kas`;
CREATE TABLE `t_kas` (
  `idkas` char(15) NOT NULL,
  `tglbk` date DEFAULT NULL,
  `nobk` char(25) DEFAULT NULL,
  `id_proyek` char(15) DEFAULT NULL,
  `koderek` char(25) DEFAULT NULL,
  `reklawan` char(25) DEFAULT NULL,
  `diserahkan` char(25) DEFAULT NULL,
  `diterima` char(25) DEFAULT NULL,
  `dicek` char(25) DEFAULT NULL,
  `disetujui` char(25) DEFAULT NULL,
  `diketahui` char(25) DEFAULT NULL,
  `tglsimpan` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `iduser` char(25) DEFAULT NULL,
  PRIMARY KEY (`idkas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_kas
-- ----------------------------

-- ----------------------------
-- Table structure for `t_kode_kas`
-- ----------------------------
DROP TABLE IF EXISTS `t_kode_kas`;
CREATE TABLE `t_kode_kas` (
  `id` int(11) NOT NULL,
  `tipe` smallint(5) NOT NULL,
  `kode` char(15) NOT NULL,
  `subkode` char(15) NOT NULL,
  `nama` char(50) NOT NULL,
  `saldoawal` int(11) NOT NULL DEFAULT '0',
  `tglsaldo` date NOT NULL DEFAULT '0000-00-00',
  `catatan` text,
  `sembunyikan` smallint(1) NOT NULL DEFAULT '0',
  `publish` char(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_kode_kas
-- ----------------------------
INSERT INTO `t_kode_kas` VALUES ('1400301786', '1', '1-1', '999', 'KAS', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400301797', '1', '1-1-001', '1-1', 'KAS', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400301829', '1', '1-2', '999', 'BANK', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400301896', '1', '1-2-001', '1-2', 'BANK BTN Syariah', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400682754', '1', '1-2-002', '1-2', 'BANK BTN Mt Haryono', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400683121', '1', '1-2-003', '1-2', 'BANK BTN KUPANG', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400683238', '1', '1-2-004', '1-2', 'BANK BTN JOGJA', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400683270', '1', '1-2-005', '1-2', 'BANK BUKOPIN', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400683358', '2', '1-3', '999', 'PIUTANG', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400683382', '2', '1-3-001', '1-3', 'PIUTANG UANG MUKA', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400683412', '2', '1-3-002', '1-3', 'PIUTANG KLT', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400683441', '2', '1-3-003', '1-3', 'PIUTANG KARYAWAN', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400683473', '2', '1-3-004', '1-3', 'PIUTANG DAJAM SERTIFIKAT', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400683508', '2', '1-3-005', '1-3', 'PIUTANG DAJAM LISTRIK', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400683588', '2', '1-3-006', '1-3', 'PIUTANG DAJAM AIR', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400683635', '2', '1-3-007', '1-3', 'PIUTANG DAJAM BISTEK', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400683669', '2', '1-3-008', '1-3', 'PIUTANG RETENSI BANGUNAN', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400683702', '3', '1-4', '999', 'PERSEDIAAN', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400683734', '3', '1-4-001', '1-4', 'PERSEDIAAN BANGUNAN JADI VILLA DELIA', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400683774', '3', '1-4-002', '1-4', 'PERSEDIAAN BANGUNAN JADI GPO', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400684319', '14', '1-5', '999', 'PAJAK DIBAYAR DI MUKA', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400684348', '14', '5-1-001', '5-1', 'PPH PASAL 21', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400684369', '14', '1-5-002', '5-1', 'PPH PASAL 23', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400684395', '14', '5-1-003', '5-1', 'PPH PASAL 4 AYAT 2 (PPH 1%)', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400685302', '4', '1-6-001', '999', 'PPN MASUKAN', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400685359', '9', '1-6-002', '999', 'PPN KELUARAN', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400685552', '5', '2-1', '999', 'PERSEDIAAN TANAH', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400685574', '5', '2-1-001', '2-1', 'TANAH KANTOR', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400685619', '5', '2-1-002', '2-1', 'ASET TANAH', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400685645', '5', '2-2', '999', 'PERSEDIAAN BANGUNAN', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400685663', '5', '2-2-001', '2-2', 'BANGUNAN KANTOR', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400685718', '5', '2-3', '999', 'INVENTARIS KANTOR', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400685745', '5', '2-3-001', '2-3', 'MOBIL', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400685775', '5', '2-3-002', '2-3', 'MOTOR', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400685803', '5', '2-3-003', '2-3', 'COMPUTER, ACC, FAN, TELP, DLL', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400685850', '5', '2-3-004', '2-3', 'MEJA KERJA, KURSI, LEMARI FILE, DLL ( FURNITURE )', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400685877', '5', '2-3-005', '2-3', 'INV LAIN-LAIN', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400685954', '6', '3-1', '999', 'AKUMULASI PENYUSUTAN AKTIVA', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400685976', '6', '3-1-001', '3-1', 'BANGUNAN KANTOR', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400685992', '6', '3-1-002', '3-1', 'PENYUSUTAN MOBIL', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400686018', '6', '3-1-003', '3-1', 'PENYUSUTAN MOTOR', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400686051', '6', '3-1-004', '3-1', 'PENYUSUTAN INVENTARIS KANTOR ( AC MEJA KURSI DLL )', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400686136', '8', '4-1', '999', 'HUTANG TANAH DAN PRODUKSI', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400686156', '8', '4-1-002', '4-1', 'HUTANG TANAH', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400686176', '8', '4-1-003', '4-1', 'HUTANG SUBKON', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400686201', '8', '4-1-004', '4-1', 'HUTANG PEMBELIAN MATERIAL', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400686344', '8', '4-1-005', '4-1', 'HUTANG BTL-LISTRIK', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400733445', '12', '8-1', '999', 'PENGHASILAN', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400733494', '12', '8-1-001', '8-1', 'PENJUALAN KPR FLPP', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400733521', '12', '8-1-002', '8-1', 'PENJUALAN KPR NON-SUBSIDI', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400733645', '12', '8-1-003', '8-1', 'PENJUALAN TUNAI', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1400734577', '14', '11-1-001', '11-1', 'GAJI DAN TUNJANGAN KARYAWAN', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1402417836', '12', '8-1-004', '8-1', 'BIAYA ADMINISTRASI RETUR', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1402418227', '12', '8-1-005', '8-1', 'RETUR PENJUALAN', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1402543998', '8', '4-4', '999', 'HUTANG MODAL PIHAK LAIN', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1402544026', '8', '4-4-001', '4-4', 'HUTANG PEMEGANG SAHAM', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1402548135', '14', '9-1', '999', 'BIAYA PRODUKSI', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1402548190', '14', '9-1-001', '9-1', 'BIAYA BELANJA MATERIAL ALAM', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1402548224', '14', '9-1-001d', '9-1-001', 'SEMEN', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1402590715', '8', '4-2', '999', 'HUTANG PENJUALAN', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1402590741', '8', '4-2-001', '4-2', 'HUTANG TITIPAN UANG MUKA', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1402590774', '8', '4-2-002', '4-2', 'HUTANG TITIPAN KLT', '0', '0000-00-00', null, '0', '1');
INSERT INTO `t_kode_kas` VALUES ('1402590800', '8', '4-2-003', '4-2', 'KOMISI MARKETING', '0', '0000-00-00', null, '0', '1');

-- ----------------------------
-- Table structure for `t_kode_titipan`
-- ----------------------------
DROP TABLE IF EXISTS `t_kode_titipan`;
CREATE TABLE `t_kode_titipan` (
  `id` char(15) NOT NULL,
  `keterangan` char(25) DEFAULT NULL,
  `publish` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_kode_titipan
-- ----------------------------
INSERT INTO `t_kode_titipan` VALUES ('1398615393', 'KLT', '1');
INSERT INTO `t_kode_titipan` VALUES ('1398615421', 'Bi PROSES', '1');
INSERT INTO `t_kode_titipan` VALUES ('1398615511', 'UANG MUKA', '1');
INSERT INTO `t_kode_titipan` VALUES ('1398615516', 'AKAD', '1');
INSERT INTO `t_kode_titipan` VALUES ('1398615675', 'AJB / NOTARIS', '1');
INSERT INTO `t_kode_titipan` VALUES ('1398615805', 'BPHTB', '1');
INSERT INTO `t_kode_titipan` VALUES ('1398615819', 'PPN 10%', '1');
INSERT INTO `t_kode_titipan` VALUES ('1398615837', 'Penambahan Mutu', '1');
INSERT INTO `t_kode_titipan` VALUES ('1398615846', 'LAIN-LAIN', '1');

-- ----------------------------
-- Table structure for `t_konsumen`
-- ----------------------------
DROP TABLE IF EXISTS `t_konsumen`;
CREATE TABLE `t_konsumen` (
  `id_konsumen` char(15) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `ktp` char(50) DEFAULT NULL,
  `masa_ktp` char(15) DEFAULT NULL,
  `alamat` char(100) DEFAULT NULL,
  `fax` char(15) DEFAULT NULL,
  `nohp` char(50) DEFAULT NULL,
  `email` char(50) DEFAULT NULL,
  `status_rmh` char(25) DEFAULT NULL,
  `blok` char(3) DEFAULT NULL,
  `pendidikan` char(15) DEFAULT NULL,
  `status` char(15) DEFAULT NULL,
  `jns_kelamin` char(15) DEFAULT NULL,
  `jml_tanggungan` char(5) DEFAULT NULL,
  `nm_ibu` char(100) DEFAULT NULL,
  `nm_x` char(100) DEFAULT NULL,
  `alamat2` char(100) DEFAULT NULL,
  `no` char(3) DEFAULT NULL,
  `rt` char(5) DEFAULT NULL,
  `rw` char(5) DEFAULT NULL,
  `kel` char(25) DEFAULT NULL,
  `kec` char(25) DEFAULT NULL,
  `kota` char(25) DEFAULT NULL,
  `prov` char(25) DEFAULT NULL,
  `kodepos` char(15) DEFAULT NULL,
  `tgl_lahir` char(15) DEFAULT NULL,
  `blok2` char(3) DEFAULT NULL,
  `no2` char(3) DEFAULT NULL,
  `rt2` char(3) DEFAULT NULL,
  `rw2` char(3) DEFAULT NULL,
  `kel2` char(25) DEFAULT NULL,
  `kec2` char(25) DEFAULT NULL,
  `kota2` char(25) DEFAULT NULL,
  `prov2` char(25) DEFAULT NULL,
  `kodepos2` char(25) DEFAULT NULL,
  `telp` char(15) DEFAULT NULL,
  `lamaditempati` char(25) DEFAULT NULL,
  `npwp` char(50) DEFAULT NULL,
  `agama` char(15) DEFAULT NULL,
  `tmpt_lahir` char(25) DEFAULT NULL,
  `hub_x` char(5) DEFAULT NULL,
  `hub_lainx` char(25) DEFAULT NULL,
  `alamat_x` char(100) DEFAULT NULL,
  `blok_x` char(3) DEFAULT NULL,
  `no_x` char(3) DEFAULT NULL,
  `rt_x` char(3) DEFAULT NULL,
  `rw_x` char(3) DEFAULT NULL,
  `kel_x` char(25) DEFAULT NULL,
  `kec_x` char(25) DEFAULT NULL,
  `kota_x` char(25) DEFAULT NULL,
  `prov_x` char(25) DEFAULT NULL,
  `kodepos_x` char(10) DEFAULT NULL,
  `telp_x` char(50) DEFAULT NULL,
  `hp_x` char(50) DEFAULT NULL,
  `nm_pasangan` char(100) DEFAULT NULL,
  `ktp_pasangan` char(50) DEFAULT NULL,
  `st_kerja_pasangan` char(1) DEFAULT NULL,
  `jns_pekerjaan` char(15) DEFAULT NULL,
  `jns_xpekerjaan` char(50) DEFAULT NULL,
  `bidang` char(25) DEFAULT NULL,
  `pangkat` char(25) DEFAULT NULL,
  `ms_kerja` char(5) DEFAULT NULL,
  `total_kerja` char(5) DEFAULT NULL,
  `nip` char(50) DEFAULT NULL,
  `nama_atasan` char(50) DEFAULT NULL,
  `telp_atasan` char(50) DEFAULT NULL,
  `hp_atasan` char(50) DEFAULT NULL,
  `nm_perusahaan2` char(50) DEFAULT NULL,
  `badanusaha2` char(15) DEFAULT NULL,
  `badanusaha2x` char(50) DEFAULT NULL,
  `alamat5` char(100) DEFAULT NULL,
  `blok5` char(3) DEFAULT NULL,
  `no5` char(3) DEFAULT NULL,
  `rt5` char(3) DEFAULT NULL,
  `rw5` char(3) DEFAULT NULL,
  `kel5` char(25) DEFAULT NULL,
  `kec5` char(25) DEFAULT NULL,
  `kota5` char(25) DEFAULT NULL,
  `prov5` char(25) DEFAULT NULL,
  `kodepos5` char(10) DEFAULT NULL,
  `telp5` char(25) DEFAULT NULL,
  `ext5` char(25) DEFAULT NULL,
  `jns_pekerjaan2` char(15) DEFAULT NULL,
  `ms_ktp_pasangan` char(15) DEFAULT NULL,
  `alamat3` char(100) DEFAULT NULL,
  `blok3` char(3) DEFAULT NULL,
  `no3` char(3) DEFAULT NULL,
  `rt3` char(3) DEFAULT NULL,
  `rw3` char(3) DEFAULT NULL,
  `kel3` char(25) DEFAULT NULL,
  `kec3` char(25) DEFAULT NULL,
  `kota3` char(25) DEFAULT NULL,
  `prov3` char(25) DEFAULT NULL,
  `kodepos3` char(10) DEFAULT NULL,
  `telp3` char(25) DEFAULT NULL,
  `hp3` char(50) DEFAULT NULL,
  `nm_perusahaan` char(50) DEFAULT NULL,
  `badanusaha` char(15) DEFAULT NULL,
  `badanusahax` char(50) DEFAULT NULL,
  `alamat4` char(100) DEFAULT NULL,
  `blok4` char(3) DEFAULT NULL,
  `no4` char(3) DEFAULT NULL,
  `rt4` char(3) DEFAULT NULL,
  `rw4` char(3) DEFAULT NULL,
  `kel4` char(25) DEFAULT NULL,
  `kec4` char(25) DEFAULT NULL,
  `kota4` char(25) DEFAULT NULL,
  `prov4` char(25) DEFAULT NULL,
  `kodepos4` char(10) DEFAULT NULL,
  `telp4` char(25) DEFAULT NULL,
  `ext4` char(25) DEFAULT NULL,
  `jns_xpekerjaan2` char(50) DEFAULT NULL,
  `bidang2` char(25) DEFAULT NULL,
  `pangkat2` char(50) DEFAULT NULL,
  `ms_kerja2` char(5) DEFAULT NULL,
  `total_kerja2` char(5) DEFAULT NULL,
  `nip2` char(50) DEFAULT NULL,
  `nama_atasan2` char(50) DEFAULT NULL,
  `telp_atasan2` char(25) DEFAULT NULL,
  `hp_atasan2` char(25) DEFAULT NULL,
  `timecreate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userid` char(25) NOT NULL,
  PRIMARY KEY (`id_konsumen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_konsumen
-- ----------------------------
INSERT INTO `t_konsumen` VALUES ('1400852705', 'BENI KURNIAWAN', null, null, 'JL. KAWUNG IX', null, '0821 33459245', null, null, '', null, null, null, null, null, null, null, '27', '14', '14', 'TLOGOSARI KULON', 'PEDURUNGAN', 'SEMARANG', 'JAWA TENGAH', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2014-05-23 20:45:05', 'admin');
INSERT INTO `t_konsumen` VALUES ('1400852805', 'HERMAN WAHYU PRATOMO', null, null, 'PERUM GROGOL INDAH', null, '085 855 165 932', null, null, '', null, null, null, null, null, null, null, '', '01', '01', 'TELUKAN', 'GROGOL', 'SUKOHARJO', 'JAWA TENGAH', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2014-05-23 20:46:45', 'admin');
INSERT INTO `t_konsumen` VALUES ('1400852906', 'AJI JOKO LELONO', '33.2214.130567.0001', '13-05-2011', 'MUNENG SIDOMULYO', '', '081 379 659 922 / 081 931 963 866', '', '1', '', '1389983352', '0', 'L', '', 'CHASBONAH', '', '', '', '02', '02', 'SIDOMULYO', 'UNGARAN TIMUR', 'SEMARANG', 'JAWA TENGAH', '50511', '', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '1', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2014-05-23 20:48:26', 'admin');
INSERT INTO `t_konsumen` VALUES ('1402033674', 'EIRENE YUSINTA', null, null, 'NONE', null, '0888 641 7547', null, null, '', null, null, null, null, null, null, null, '', '', '', '', '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2014-06-06 12:47:54', 'admin');

-- ----------------------------
-- Table structure for `t_maintenance`
-- ----------------------------
DROP TABLE IF EXISTS `t_maintenance`;
CREATE TABLE `t_maintenance` (
  `id` smallint(4) NOT NULL AUTO_INCREMENT,
  `title` char(50) DEFAULT NULL,
  `subtitle` char(100) DEFAULT NULL,
  `alamat` text,
  `telp` char(100) DEFAULT NULL,
  `fax` char(100) DEFAULT NULL,
  `sms` char(15) DEFAULT NULL,
  `url` char(50) DEFAULT NULL,
  `email` char(50) DEFAULT NULL,
  `pagesyarat` text,
  `beranda` text,
  `publish` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_maintenance
-- ----------------------------
INSERT INTO `t_maintenance` VALUES ('1', 'PT. PRIMA INDO MEGAH', 'SISTEM INFORMASI KEUANGAN', '', '', '', null, '', '', '&lt;p&gt;syarat&lt;/p&gt;', '&lt;h1 style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;background-color: #ffffff;&quot;&gt;PENGUMUMAN BERANDA SEKARANG&lt;/span&gt; &lt;em&gt;&lt;strong&gt;&lt;span style=&quot;color: #ff0000;&quot;&gt;TIDAK ADA&lt;/span&gt;&lt;/strong&gt;&lt;/em&gt; !!!!&lt;/h1&gt;\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;h4 style=&quot;text-align: center;&quot;&gt;&lt;strong&gt;Atas Perhatiannya kami ucapkan terima kasih. &lt;/strong&gt;&lt;img src=&quot;js/tinymce/plugins/emoticons/img/smiley-laughing.gif&quot; alt=&quot;&quot; /&gt;&lt;/h4&gt;', '1');

-- ----------------------------
-- Table structure for `t_main_menu`
-- ----------------------------
DROP TABLE IF EXISTS `t_main_menu`;
CREATE TABLE `t_main_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_submenu` int(11) DEFAULT NULL,
  `menu` char(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `module` varchar(50) DEFAULT NULL,
  `urutan` tinyint(5) DEFAULT NULL,
  `tipe` char(10) DEFAULT NULL,
  `submenu` tinyint(1) DEFAULT '1',
  `toolbar` tinyint(1) DEFAULT '0',
  `publish` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_main_menu
-- ----------------------------
INSERT INTO `t_main_menu` VALUES ('1', '0', 'beranda', 'Dashboard', '/dashboard/', null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('48', '0', null, 'PENGATURAN DATABASE', null, null, null, '1', '0', '1');
INSERT INTO `t_main_menu` VALUES ('49', '48', null, 'Management Database', null, null, null, '1', '0', '1');
INSERT INTO `t_main_menu` VALUES ('50', '49', 'proyek', 'DATABASE PROYEK', '/data/proyek/', null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('51', '49', 'siteplan', 'DATABASE KAPLING', '/data/siteplan/', null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('53', '49', 'konsumen', 'Data Penjualan', '/konsumen/', null, null, '0', '0', '0');
INSERT INTO `t_main_menu` VALUES ('54', '49', 'pengguna', 'DATABASE PENGGUNA', '/pengguna/', null, null, '0', '0', '0');
INSERT INTO `t_main_menu` VALUES ('55', '48', 'datakaryawan', 'Data Karyawan Pengguna', '/data/karyawan/', null, null, '0', '0', '0');
INSERT INTO `t_main_menu` VALUES ('56', '0', null, 'MARKETING', null, null, null, '1', '0', '1');
INSERT INTO `t_main_menu` VALUES ('57', '56', '', 'Penjualan', '', null, null, '1', '0', '0');
INSERT INTO `t_main_menu` VALUES ('58', '49', 'jenispekerjaan', 'DATABASE PEKERJAAN', '/data/form/pekerjaan/', null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('59', '49', 'bank', 'DATABASE BANK dan PERSYARATAN BANK', '/data/bank/', null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('60', '49', 'umlain', 'DATABASE UANG MUKA LAIN', '/data/form/uangmuka/', null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('61', '49', 'syaratbank', 'Data Persyaratan Bank', '/data/form/syaratbank/', null, null, '0', '0', '0');
INSERT INTO `t_main_menu` VALUES ('62', '49', 'syaratpersonal', 'DATABASE PERSYARATAN PERSONAL', '/data/form/syaratpersonal/', null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('63', '0', null, 'KEUANGAN', null, null, null, '1', '0', '1');
INSERT INTO `t_main_menu` VALUES ('64', '90', '', 'Laporan Marketing', null, null, null, '1', '0', '0');
INSERT INTO `t_main_menu` VALUES ('65', '64', 'marketing/report', 'KPR', null, null, null, '0', '0', '0');
INSERT INTO `t_main_menu` VALUES ('66', '65', 'lapkprmarprapen', 'Laporan Pra Penjualan', '/laporan/marketing/kpr/prapenjualan/', null, null, '0', '0', '0');
INSERT INTO `t_main_menu` VALUES ('67', '65', 'marketing/report', 'Laporan Penjualan', '/laporan/marketing/kpr/penjualan/', null, null, '0', '0', '0');
INSERT INTO `t_main_menu` VALUES ('68', '90', null, 'Laporan Keuangan', null, null, null, '1', '0', '0');
INSERT INTO `t_main_menu` VALUES ('69', '68', 'keuangan/report', 'KPR', null, null, null, '0', '0', '0');
INSERT INTO `t_main_menu` VALUES ('70', '69', 'lapkprkeuprapen', 'Laporan Pra Penjualan', '/laporan/penjualan/kpr/prapenjualan/', null, null, '0', '0', '0');
INSERT INTO `t_main_menu` VALUES ('71', '69', 'lapkprkeupen', 'Laporan Penjualan', '/laporan/penjualan/kpr/penjualan/', null, null, '0', '0', '0');
INSERT INTO `t_main_menu` VALUES ('72', '64', 'marketing/piutang', 'Laporan Piutang', '/laporan/marketing/piutang/', null, null, '0', '0', '0');
INSERT INTO `t_main_menu` VALUES ('73', '64', 'marketing/pphfinalbphtb', 'Laporan PPH FINAL & BPHTB', '/laporan/marketing/pphfinal/', null, null, '0', '0', '0');
INSERT INTO `t_main_menu` VALUES ('74', '90', 'rdatakonsumen', 'Laporan Data Konsumen', '/laporan/datakonsumen/', null, null, '0', '0', '0');
INSERT INTO `t_main_menu` VALUES ('75', '48', 'profile', 'My Profile', '/profile/', null, null, '0', '0', '0');
INSERT INTO `t_main_menu` VALUES ('76', '56', '', 'INPUT PENJUALAN', '/input/formpenjualan/', null, null, '1', '0', '0');
INSERT INTO `t_main_menu` VALUES ('77', '76', 'formpenjualan/kredit', 'KREDIT', null, null, null, '0', '0', '0');
INSERT INTO `t_main_menu` VALUES ('78', '49', null, 'Database Form', null, null, null, '1', '0', '0');
INSERT INTO `t_main_menu` VALUES ('79', '49', 'jabatan', 'DATABASE JABATAN', '/data/form/jabatan/', null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('80', '78', 'pendidikan', 'Data Pendidikan', '/data/form/pendidikan/', null, null, '0', '0', '0');
INSERT INTO `t_main_menu` VALUES ('81', '48', null, 'Management Website', null, null, null, '1', '0', '1');
INSERT INTO `t_main_menu` VALUES ('82', '81', 'site', 'Pengaturan Site', '/data/site/', null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('83', '81', 'info', 'Informasi Website', '/data/informasi/', null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('84', '0', 'glyphicon glyphicon-list-alt', 'Data Pribadi', '/profile/mine/', null, null, '0', '1', '0');
INSERT INTO `t_main_menu` VALUES ('85', '0', 'ubahpassword', 'Ubah Password', '/profile/ubahpassword/', null, null, '0', '1', '1');
INSERT INTO `t_main_menu` VALUES ('86', '81', null, 'Tracking Website', '/pengaturan/track/', null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('87', '56', 'formpenjualan', 'CARI dan EDIT PENJUALAN', '/input/editformpenjualan/', null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('88', '76', 'formpenjualan/tunai', 'TUNAI', null, null, null, '0', '0', '0');
INSERT INTO `t_main_menu` VALUES ('89', '63', 'kodekaskeuangan', 'DAFTAR KODE PERKIRAAN', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('90', '0', '', 'PELAPORAN', null, null, null, '1', '0', '1');
INSERT INTO `t_main_menu` VALUES ('91', '63', 'datatitipan', 'INPUT DATA TITIPAN', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('92', '63', '', 'KAS DAN BANK', null, null, null, '1', '0', '1');
INSERT INTO `t_main_menu` VALUES ('93', '56', 'datamarketing', 'DATABASE MARKETING', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('94', '56', 'stsmarketing', 'DATABASE STATUS MARKETING', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('95', '48', 'datapengguna', 'DATA PENGGUNA', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('96', '63', 'kodetitipan', 'KODE TITIPAN', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('97', '63', 'inputdatajual', 'INPUT DATA KONSUMEN', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('98', '56', 'accproses', 'PROSES ACC KONSUMEN', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('99', '56', 'prosesfinal', 'PROSES AKAD / AJB', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('100', '90', 'prajualmarketing', 'LAPORAN PRA PENJUALAN MARKETING', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('101', '90', 'jualmarketing', 'LAPORAN PENJUALAN MARKETING', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('102', '90', 'laporantk', 'LAPORAN UANG TITIPAN', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('103', '90', 'lapkasharian', 'LAPORAN KAS HARIAN', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('104', '92', 'kasmasuk', 'DAFTAR KAS MASUK', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('105', '92', 'kaskeluar', 'DAFTAR KAS KELUAR', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('106', '92', 'bukubank', 'BUKU BANK', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('107', '109', 'jurnalvoucher', 'BUKTI JURNAL UMUM', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('108', '0', null, 'DAFTAR', null, null, null, '1', '0', '1');
INSERT INTO `t_main_menu` VALUES ('109', '108', 'bukubesar', 'BUKU BESAR', null, null, null, '1', '0', '1');
INSERT INTO `t_main_menu` VALUES ('110', '109', 'historibukubesar', 'RIWAYAT BUKU BESAR', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('111', '90', null, 'BUKU BESAR', null, null, null, '1', '0', '1');
INSERT INTO `t_main_menu` VALUES ('112', '111', 'seluruhjurnal', 'JURNAL UMUM', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('113', '111', 'rincibukubesar', 'RINCIAN BUKU BESAR', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('114', '111', 'neracasaldo', 'NERACA SALDO', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('115', '108', '', 'KONSUMEN', null, null, null, '1', '0', '1');
INSERT INTO `t_main_menu` VALUES ('116', '115', 'inputkonsumen', 'INPUT KONSUMEN', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('117', '108', '', 'PENJUALAN', null, null, null, '1', '0', '1');
INSERT INTO `t_main_menu` VALUES ('118', '117', 'prapenjualan', 'INPUT PENJUALAN', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('119', '117', 'ubahcaridaftarjual', 'UBAH dan CARI', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('120', '117', 'aksiacc', 'AKSI ACC', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('121', '117', 'aksifinal', 'AKSI FINAL', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('122', '115', 'ubahcarikonsumen', 'UBAH DAN CARI', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('125', '117', 'fakturjual', 'FAKTUR PENJUALAN', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('126', '117', 'retur', 'RETUR PENJUALAN', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('127', '117', 'terimajual', 'PENERIMAAN PENJUALAN', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('128', '117', 'hutangjual', 'HUTANG PENJUALAN', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('129', '108', '', 'LOGISTIK', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('130', '129', 'createrab', 'BUAT RAB', null, null, null, '0', '0', '1');
INSERT INTO `t_main_menu` VALUES ('131', '129', 'updaterab', 'CARI dan EDIT RAB', null, null, null, '0', '0', '1');

-- ----------------------------
-- Table structure for `t_main_menu2`
-- ----------------------------
DROP TABLE IF EXISTS `t_main_menu2`;
CREATE TABLE `t_main_menu2` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_submenu` int(11) DEFAULT NULL,
  `menu` char(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `module` varchar(50) DEFAULT NULL,
  `urutan` tinyint(5) DEFAULT NULL,
  `tipe` char(10) DEFAULT NULL,
  `submenu` tinyint(1) DEFAULT '1',
  `toolbar` tinyint(1) DEFAULT '0',
  `publish` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_main_menu2
-- ----------------------------

-- ----------------------------
-- Table structure for `t_mandor`
-- ----------------------------
DROP TABLE IF EXISTS `t_mandor`;
CREATE TABLE `t_mandor` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_mandor
-- ----------------------------
INSERT INTO `t_mandor` VALUES ('1405519203', 'satu orang');
INSERT INTO `t_mandor` VALUES ('1405548456', 'mandor dua');
INSERT INTO `t_mandor` VALUES ('1405548471', 'mandor tiga');

-- ----------------------------
-- Table structure for `t_marketing`
-- ----------------------------
DROP TABLE IF EXISTS `t_marketing`;
CREATE TABLE `t_marketing` (
  `id` char(15) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` char(100) DEFAULT NULL,
  `blok` char(3) DEFAULT NULL,
  `no` char(3) DEFAULT NULL,
  `rt` char(5) DEFAULT NULL,
  `rw` char(5) DEFAULT NULL,
  `kel` char(25) DEFAULT NULL,
  `kec` char(25) DEFAULT NULL,
  `kota` char(25) DEFAULT NULL,
  `prov` char(25) DEFAULT NULL,
  `kodepos` char(15) DEFAULT NULL,
  `alamat2` char(100) DEFAULT NULL,
  `blok2` char(3) DEFAULT NULL,
  `no2` char(3) DEFAULT NULL,
  `rt2` char(5) DEFAULT NULL,
  `rw2` char(5) DEFAULT NULL,
  `kel2` char(25) DEFAULT NULL,
  `kec2` char(25) DEFAULT NULL,
  `kota2` char(25) DEFAULT NULL,
  `prov2` char(25) DEFAULT NULL,
  `kodepos2` char(15) DEFAULT NULL,
  `jns_kelamin` char(15) DEFAULT NULL,
  `ktp` char(50) DEFAULT NULL,
  `npwp` char(50) DEFAULT NULL,
  `telp` char(15) DEFAULT NULL,
  `nohp` char(50) DEFAULT NULL,
  `email` char(50) DEFAULT NULL,
  `idsts` char(15) DEFAULT NULL,
  `tglinput` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `userid` char(25) DEFAULT NULL,
  `last_update` char(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_marketing
-- ----------------------------
INSERT INTO `t_marketing` VALUES ('1399274300', 'DIAN NOERHAYATI', 'KENDALI SODO', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'P', '888', '', '', '085866110444', '', '1399273523', '2014-05-05 14:18:20', 'backoffice', '2014-05-08 11:1');
INSERT INTO `t_marketing` VALUES ('1399521000', 'ADI', 'YKPP KEMHAM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'L', '333', '', '', '0813 6540 7372', '', '1399273426', '2014-05-08 10:50:00', 'backoffice', null);
INSERT INTO `t_marketing` VALUES ('1399522147', 'TANTY', 'WIDURI', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'P', '777', '', '', '08882409191', '', '1397913214', '2014-05-08 11:09:07', 'backoffice', null);
INSERT INTO `t_marketing` VALUES ('1399522241', 'ANNA', 'GENID', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'P', '111', '', '', '0823335222210', '', '1397913214', '2014-05-08 11:10:41', 'backoffice', null);
INSERT INTO `t_marketing` VALUES ('1399522294', 'PUGUH', 'BANGANOM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'L', '222', '', '', '082138077328', '', '1397913214', '2014-05-08 11:11:34', 'backoffice', null);
INSERT INTO `t_marketing` VALUES ('1399522362', 'WAWAN', 'DEMAK', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'L', '444', '', '', '081227317072', '', '1397913214', '2014-05-08 11:12:42', 'backoffice', null);
INSERT INTO `t_marketing` VALUES ('1399522427', 'YOYOK', 'YOGYA', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'L', '555', '', '', '085782575269', '', '1397913214', '2014-05-08 11:13:47', 'backoffice', null);
INSERT INTO `t_marketing` VALUES ('1399522579', 'AGUSTINUS', 'GRAHAMUKTI', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'L', '6666', '', '', '081326600100', '', '1397913214', '2014-05-08 11:16:19', 'backoffice', null);
INSERT INTO `t_marketing` VALUES ('1400035351', 'KANTOR', 'PIM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'P', '999', '', '', '999', '', '1397913214', '2014-05-14 09:42:31', 'backoffice', null);

-- ----------------------------
-- Table structure for `t_pengaturan_lain`
-- ----------------------------
DROP TABLE IF EXISTS `t_pengaturan_lain`;
CREATE TABLE `t_pengaturan_lain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `debetretur` char(15) DEFAULT NULL,
  `kreditretur` char(15) DEFAULT NULL,
  `debetbiaya` char(15) DEFAULT NULL,
  `kreditbiaya` char(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_pengaturan_lain
-- ----------------------------
INSERT INTO `t_pengaturan_lain` VALUES ('1', '1402418227', '1400301797', '1400301797', '1402417836');

-- ----------------------------
-- Table structure for `t_penghasilan`
-- ----------------------------
DROP TABLE IF EXISTS `t_penghasilan`;
CREATE TABLE `t_penghasilan` (
  `id_penghasilan` char(15) NOT NULL,
  `id_penjualan` char(15) DEFAULT NULL,
  `nama_penghasilan` char(50) DEFAULT NULL,
  `jml_penghasilan` char(15) DEFAULT NULL,
  PRIMARY KEY (`id_penghasilan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_penghasilan
-- ----------------------------
INSERT INTO `t_penghasilan` VALUES ('13984741131', '1398474113', 'GAJI POKOK', '1390000');
INSERT INTO `t_penghasilan` VALUES ('1398475341', '1398474113', 'TUNJANGAN JABATAN', '200000');
INSERT INTO `t_penghasilan` VALUES ('1398475373', '1398474113', 'HONOR', '75000');
INSERT INTO `t_penghasilan` VALUES ('1398475480', '1398474113', 'UM', '330000');
INSERT INTO `t_penghasilan` VALUES ('1398475503', '1398474113', 'HONOR KEPANITIAAN', '500000');
INSERT INTO `t_penghasilan` VALUES ('1398475530', '1398474113', 'LAIN LAIN', '300000');
INSERT INTO `t_penghasilan` VALUES ('1398751308', '1398542286', 'GAJI POKOK', '1758200');
INSERT INTO `t_penghasilan` VALUES ('1398751338', '1398542286', 'TUNJ JABATAN FUNGSIONAL', '75000');
INSERT INTO `t_penghasilan` VALUES ('1398751366', '1398542286', 'TUNJ BERAS', '101808');
INSERT INTO `t_penghasilan` VALUES ('1398751382', '1398542286', 'TUNJ PEMBULATAN', '12');
INSERT INTO `t_penghasilan` VALUES ('1398751402', '1398542286', 'ULP', '1240000');
INSERT INTO `t_penghasilan` VALUES ('1398751425', '1398542286', 'PPH21', '37737');
INSERT INTO `t_penghasilan` VALUES ('1398755951', '1398567196', 'GAJ', '1');
INSERT INTO `t_penghasilan` VALUES ('1399004669', '1398539777', 'GAJI POKOK', '1200000');
INSERT INTO `t_penghasilan` VALUES ('1399004693', '1398539777', 'TUNJ MAKAN', '250000');
INSERT INTO `t_penghasilan` VALUES ('1399004723', '1398539777', 'TUNJ TRANSPOSRT', '500000');
INSERT INTO `t_penghasilan` VALUES ('1399004737', '1398539777', 'TUNJ JABATAN', '500000');
INSERT INTO `t_penghasilan` VALUES ('1401088093', '1400852906', 'GAJI POKOK', '3000000');

-- ----------------------------
-- Table structure for `t_penjualan`
-- ----------------------------
DROP TABLE IF EXISTS `t_penjualan`;
CREATE TABLE `t_penjualan` (
  `id` char(15) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `ktp` char(50) DEFAULT NULL,
  `masa_ktp` char(15) DEFAULT NULL,
  `alamat` char(100) DEFAULT NULL,
  `fax` char(15) DEFAULT NULL,
  `nohp` char(50) DEFAULT NULL,
  `email` char(50) DEFAULT NULL,
  `status_rmh` char(25) DEFAULT NULL,
  `hargajual` char(15) DEFAULT NULL,
  `jns_kredit` char(10) DEFAULT NULL,
  `jml_angsur_kredit` char(5) DEFAULT NULL,
  `nilai_angsur_kredit` char(15) DEFAULT NULL,
  `kemampuan_angsur` char(15) DEFAULT NULL,
  `id_proyek` char(15) DEFAULT NULL,
  `id_kapling` char(15) DEFAULT NULL,
  `type_kapling` char(5) DEFAULT NULL,
  `jns_penjualan` char(15) DEFAULT NULL,
  `blok` char(3) DEFAULT NULL,
  `pendidikan` char(15) DEFAULT NULL,
  `status` char(15) DEFAULT NULL,
  `jns_kelamin` char(15) DEFAULT NULL,
  `jml_tanggungan` char(5) DEFAULT NULL,
  `nm_ibu` char(100) DEFAULT NULL,
  `nm_x` char(100) DEFAULT NULL,
  `alamat2` char(100) DEFAULT NULL,
  `no` char(3) DEFAULT NULL,
  `rt` char(5) DEFAULT NULL,
  `rw` char(5) DEFAULT NULL,
  `kel` char(25) DEFAULT NULL,
  `kec` char(25) DEFAULT NULL,
  `kota` char(25) DEFAULT NULL,
  `prov` char(25) DEFAULT NULL,
  `kodepos` char(15) DEFAULT NULL,
  `tgl_lahir` char(15) DEFAULT NULL,
  `blok2` char(3) DEFAULT NULL,
  `no2` char(3) DEFAULT NULL,
  `rt2` char(3) DEFAULT NULL,
  `rw2` char(3) DEFAULT NULL,
  `kel2` char(25) DEFAULT NULL,
  `kec2` char(25) DEFAULT NULL,
  `kota2` char(25) DEFAULT NULL,
  `prov2` char(25) DEFAULT NULL,
  `kodepos2` char(25) DEFAULT NULL,
  `telp` char(15) DEFAULT NULL,
  `lamaditempati` char(25) DEFAULT NULL,
  `npwp` char(50) DEFAULT NULL,
  `agama` char(15) DEFAULT NULL,
  `tmpt_lahir` char(25) DEFAULT NULL,
  `hub_x` char(5) DEFAULT NULL,
  `hub_lainx` char(25) DEFAULT NULL,
  `alamat_x` char(100) DEFAULT NULL,
  `blok_x` char(3) DEFAULT NULL,
  `no_x` char(3) DEFAULT NULL,
  `rt_x` char(3) DEFAULT NULL,
  `rw_x` char(3) DEFAULT NULL,
  `kel_x` char(25) DEFAULT NULL,
  `kec_x` char(25) DEFAULT NULL,
  `kota_x` char(25) DEFAULT NULL,
  `prov_x` char(25) DEFAULT NULL,
  `kodepos_x` char(10) DEFAULT NULL,
  `telp_x` char(50) DEFAULT NULL,
  `hp_x` char(50) DEFAULT NULL,
  `nm_pasangan` char(100) DEFAULT NULL,
  `ktp_pasangan` char(50) DEFAULT NULL,
  `st_kerja_pasangan` char(1) DEFAULT NULL,
  `jns_pekerjaan` char(15) DEFAULT NULL,
  `jns_xpekerjaan` char(50) DEFAULT NULL,
  `bidang` char(25) DEFAULT NULL,
  `pangkat` char(25) DEFAULT NULL,
  `ms_kerja` char(5) DEFAULT NULL,
  `total_kerja` char(5) DEFAULT NULL,
  `nip` char(50) DEFAULT NULL,
  `nama_atasan` char(50) DEFAULT NULL,
  `telp_atasan` char(50) DEFAULT NULL,
  `hp_atasan` char(50) DEFAULT NULL,
  `nm_perusahaan2` char(50) DEFAULT NULL,
  `badanusaha2` char(15) DEFAULT NULL,
  `badanusaha2x` char(50) DEFAULT NULL,
  `alamat5` char(100) DEFAULT NULL,
  `blok5` char(3) DEFAULT NULL,
  `no5` char(3) DEFAULT NULL,
  `rt5` char(3) DEFAULT NULL,
  `rw5` char(3) DEFAULT NULL,
  `kel5` char(25) DEFAULT NULL,
  `kec5` char(25) DEFAULT NULL,
  `kota5` char(25) DEFAULT NULL,
  `prov5` char(25) DEFAULT NULL,
  `kodepos5` char(10) DEFAULT NULL,
  `telp5` char(25) DEFAULT NULL,
  `ext5` char(25) DEFAULT NULL,
  `jns_pekerjaan2` char(15) DEFAULT NULL,
  `ms_ktp_pasangan` char(15) DEFAULT NULL,
  `alamat3` char(100) DEFAULT NULL,
  `blok3` char(3) DEFAULT NULL,
  `no3` char(3) DEFAULT NULL,
  `rt3` char(3) DEFAULT NULL,
  `rw3` char(3) DEFAULT NULL,
  `kel3` char(25) DEFAULT NULL,
  `kec3` char(25) DEFAULT NULL,
  `kota3` char(25) DEFAULT NULL,
  `prov3` char(25) DEFAULT NULL,
  `kodepos3` char(10) DEFAULT NULL,
  `telp3` char(25) DEFAULT NULL,
  `hp3` char(50) DEFAULT NULL,
  `nm_perusahaan` char(50) DEFAULT NULL,
  `badanusaha` char(15) DEFAULT NULL,
  `badanusahax` char(50) DEFAULT NULL,
  `alamat4` char(100) DEFAULT NULL,
  `blok4` char(3) DEFAULT NULL,
  `no4` char(3) DEFAULT NULL,
  `rt4` char(3) DEFAULT NULL,
  `rw4` char(3) DEFAULT NULL,
  `kel4` char(25) DEFAULT NULL,
  `kec4` char(25) DEFAULT NULL,
  `kota4` char(25) DEFAULT NULL,
  `prov4` char(25) DEFAULT NULL,
  `kodepos4` char(10) DEFAULT NULL,
  `telp4` char(25) DEFAULT NULL,
  `ext4` char(25) DEFAULT NULL,
  `jns_xpekerjaan2` char(50) DEFAULT NULL,
  `bidang2` char(25) DEFAULT NULL,
  `pangkat2` char(50) DEFAULT NULL,
  `ms_kerja2` char(5) DEFAULT NULL,
  `total_kerja2` char(5) DEFAULT NULL,
  `nip2` char(50) DEFAULT NULL,
  `nama_atasan2` char(50) DEFAULT NULL,
  `telp_atasan2` char(25) DEFAULT NULL,
  `hp_atasan2` char(25) DEFAULT NULL,
  `rekening` char(50) DEFAULT NULL,
  `angsur_via` char(50) DEFAULT NULL,
  `id_bank` char(15) DEFAULT NULL,
  `syarat_personal` text,
  `syarat_bank` text,
  `id_marketing` char(25) DEFAULT NULL,
  `infoum` char(15) DEFAULT NULL,
  `idumx` char(15) DEFAULT NULL,
  `namaumx` char(25) DEFAULT NULL,
  `infoumx` char(15) DEFAULT NULL,
  `infoakad` char(15) DEFAULT NULL,
  `infonotaris` char(15) DEFAULT NULL,
  `infobphtb` char(15) DEFAULT NULL,
  `infoppnpajak` char(15) DEFAULT NULL,
  `infokltpermeter` char(15) DEFAULT NULL,
  `infoklt` char(15) DEFAULT NULL,
  `infoppnklt` char(15) DEFAULT NULL,
  `infomutu` char(15) DEFAULT NULL,
  `infoppnmutu` char(15) DEFAULT NULL,
  `tglbf` date DEFAULT '0000-00-00',
  `hargajual2` char(15) DEFAULT NULL,
  `infoum2` char(15) DEFAULT NULL,
  `jml_angsur_kredit2` char(5) DEFAULT NULL,
  `nilai_angsur_kredit2` char(15) DEFAULT NULL,
  `ketacc` text,
  `tglacc` date DEFAULT '0000-00-00',
  `useracc` char(25) DEFAULT NULL,
  `tglakad` date DEFAULT '0000-00-00',
  `ketakad` text,
  `userakad` char(25) DEFAULT NULL,
  `proses` char(5) DEFAULT NULL,
  `imb` char(25) DEFAULT NULL,
  `luas_sebenarnya` char(15) DEFAULT '0',
  `tglinput` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `userid` char(25) DEFAULT NULL,
  `last_update` char(15) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_kapling` (`id_kapling`),
  CONSTRAINT `t_penjualan_ibfk_1` FOREIGN KEY (`id_kapling`) REFERENCES `t_kapling` (`id_kapling`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_penjualan
-- ----------------------------
INSERT INTO `t_penjualan` VALUES ('1398474113', 'ELFIRA SUSANTI', '337408701088 0001', '30-10-2016', 'PUDAK PAYUNG', '', '085 7268 82585', '', '3', '88000000', '021', '15', '734300', '0', '1393089671', '13930896895', '36', '02', '', '1389983384', '0', 'P', '', 'SRI WAHYUNI', 'PAHALA WIRAWAN ADI', '', '', '04', '04', '', 'BANYUMANIK', 'SEMARANG', 'JAWA TENGAH', '', '30-10-1988', '', '', '', '', '', '', '', '', '', '', '', '54 715 150 6 517 000', '1', 'SEMARANG', '3', '', 'JL. SLAMET RIYADI', '', '21', '04', '01', 'GENUK BARAT', 'UNGARAN BARAT', 'UNGARAN', 'JAWA TENGAH', '', '', '085 640 310659', 'COBA PASANGAN', '', '0', '1395159962', '', '', 'ADMINISTRASI', '3', '3', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', 'FAK. KEDOKTERAN UNISULLA', '999', 'UNIVERSITAS', 'JL. KALIGAWE KM 4 TERBOYO SMG', '', '', '', '', '', '', '', '', '', '024 658 3584', '', '', '', '', '', '', '', '', '', '', '', '', '1393110405', '1382757051', '1393130283', '1398474097', '3500000', '1', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2014-05-04', null, null, null, null, null, null, null, '0000-00-00', null, null, '131', '', '66', '2014-04-26 08:01:53', 'admin', '1398474113');
INSERT INTO `t_penjualan` VALUES ('1398539777', 'FEBRIANTO SETYADI', '33 7413 170281 0003', '17-02-2014', 'TEGAL KANGKUNG', '', '085 640 960 961', '', '3', '55000000', '021', '15', '481432', '0', '1393089671', '13930896937', '30', '02', 'XV', '1389983384', '0', 'L', '', 'MARIA SUSILAWATI', '', '', '155', '02', '02', 'KEDUNG MUNDU', 'TEMBALANG', 'SEMARANG', 'JAWA TENGAH', '50273', '17-02-1981', '', '', '', '', '', '', '', '', '', '', '', '36 575 422 5 517 000', '1', 'PEKALONGAN', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '1395159962', '', '', 'BAG. KEUANGAN', '2', '2', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', 'CAHYA SEMERU', '2', '', 'JL. ELANGSARI SELATAN', 'III', '36', '', '', '', '', 'SEMARANG', 'JAWA TENGAH', '', '', '', '', '', '', '', '', '', '', '', '', '00485 01 50 000139 6', 'Via Bank', '1393110405', '1382757051,1382853177,1393110372', '1393130283,1393131059,1393131095', '1398474097', '6000000', '1', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2014-05-04', null, null, null, null, null, null, null, null, null, null, '131', '', '66', '2014-04-27 02:16:17', 'admin', '1398539777');
INSERT INTO `t_penjualan` VALUES ('1398542286', 'HERMAN WAHYU PRATOMO', null, null, 'PERUM GROROL INDAH', null, '085 855 165 932', null, null, null, '', null, null, null, '1393089671', '13930896883', null, '01', '', null, null, null, null, null, null, null, '', '01', '01', 'TELUKAN', 'GROGOL', 'SUKOHARJO', 'JAWA TENGAH', '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2014-05-04', null, null, null, null, null, null, null, '2014-04-29', '', 'admin', '133', null, '0', '2014-04-27 02:58:06', 'admin', '1398542286');
INSERT INTO `t_penjualan` VALUES ('1398567196', 'IWAN BAGUS PUTRANTO', '1', '12-12-2012', 'JL. WARU BUNCIT 12 MAMPANG', '', '085 888 462 046', '', '1', '88000000', '021', '12', '3200', '0', '1393089671', '13930896750', '30', '02', '', '1389983352', '0', 'P', '', '', '', '', '34A', '', '', '', '', '', 'DKI JAKARTA', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '1', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '1393110405', '1382757051,1382853177,1393110372,1393557922,1393557943,1393557899,1393557959,1393558018,1393321618,1393110388,1393110354', '1393130283,1393131059', '1398474097', '9000000', '1', '', '', '0', '0', '0', '0', '1000', '10000', '1000', '1000000', '100000', '2014-05-04', '88000000', '9000000', '12', '3200', 'Keterangan', '2014-04-29', 'admin', null, null, null, '132', '', '66', '2014-04-27 09:53:17', 'admin', '1398567196');

-- ----------------------------
-- Table structure for `t_penjualan2`
-- ----------------------------
DROP TABLE IF EXISTS `t_penjualan2`;
CREATE TABLE `t_penjualan2` (
  `id_penjualan` char(15) NOT NULL,
  `id_konsumen` char(15) NOT NULL,
  `jns_penjualan` char(15) NOT NULL,
  `jns_kredit` char(10) DEFAULT NULL,
  `id_proyek` char(15) NOT NULL,
  `id_kapling` char(15) NOT NULL,
  `id_marketing` char(25) NOT NULL,
  `hargajual` char(15) DEFAULT NULL,
  `jml_angsur_kredit` char(5) DEFAULT NULL,
  `nilai_angsur_kredit` char(15) DEFAULT NULL,
  `kemampuan_angsur` char(15) DEFAULT NULL,
  `type_kapling` char(5) DEFAULT NULL,
  `rekening` char(50) DEFAULT NULL,
  `angsur_via` char(50) DEFAULT NULL,
  `id_bank` char(15) DEFAULT NULL,
  `syarat_personal` text,
  `syarat_bank` text,
  `infoum` char(15) DEFAULT NULL,
  `idumx` char(15) DEFAULT NULL,
  `namaumx` char(25) DEFAULT NULL,
  `infoumx` char(15) DEFAULT NULL,
  `infoakad` char(15) DEFAULT NULL,
  `infonotaris` char(15) DEFAULT NULL,
  `infobphtb` char(15) DEFAULT NULL,
  `infoppnpajak` char(15) DEFAULT NULL,
  `infokltpermeter` char(15) DEFAULT NULL,
  `infoklt` char(15) DEFAULT NULL,
  `infoppnklt` char(15) DEFAULT NULL,
  `infomutu` char(15) DEFAULT NULL,
  `infoppnmutu` char(15) DEFAULT NULL,
  `tglbf` date DEFAULT '0000-00-00',
  `hargajual2` char(15) DEFAULT NULL,
  `infoum2` char(15) DEFAULT NULL,
  `jml_angsur_kredit2` char(5) DEFAULT NULL,
  `nilai_angsur_kredit2` char(15) DEFAULT NULL,
  `ketacc` text,
  `tglacc` date DEFAULT '0000-00-00',
  `useracc` char(25) DEFAULT NULL,
  `tglakad` date DEFAULT '0000-00-00',
  `ketakad` text,
  `userakad` char(25) DEFAULT NULL,
  `proses` char(5) DEFAULT NULL,
  `retur` smallint(6) DEFAULT '0',
  `tglretur` date DEFAULT '0000-00-00',
  `jmlretur` int(11) DEFAULT '0',
  `chkpotongan` smallint(1) DEFAULT '0',
  `biayaretur` int(11) DEFAULT '0',
  `ketretur` text,
  `imb` char(25) DEFAULT NULL,
  `luas_sebenarnya` char(15) DEFAULT '0',
  `userid` char(25) NOT NULL,
  `last_update` char(15) NOT NULL,
  PRIMARY KEY (`id_penjualan`),
  KEY `id_kapling` (`id_kapling`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_penjualan2
-- ----------------------------
INSERT INTO `t_penjualan2` VALUES ('1400952212', '1400852805', '01', '', '1393089671', '13930896883', '1398474097', '58000000', '', '', '0', '30', '', '', '1393110405', '', '', '0', '1', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '2014-05-24', null, null, null, null, null, '0000-00-00', null, '2014-06-12', 'LUNAS', 'admin', '133', '0', '0000-00-00', '0', '0', '0', null, '', '66', 'admin', '1401070142');
INSERT INTO `t_penjualan2` VALUES ('1400952318', '1400852705', '02', '021', '1393089671', '13930896750', '1398474097', '88000000', '10', '600000', '0', '36', '', '', '1393110405', '1382757051', '1393130283,1393131059', '3500000', '1', '', '', '1', '2', '3', '4', '1000000', '10000000', '1000000', '5', '1', '2014-05-24', null, null, null, null, null, '0000-00-00', null, '0000-00-00', null, null, '131', '0', '0000-00-00', '0', '0', '0', null, '', '66', 'admin', '1401251932');
INSERT INTO `t_penjualan2` VALUES ('1400952401', '1400852906', '02', '021', '1393089671', '13930896920', '1398474097', '55000000', '8', '690466', '0', '30', '', '', '1393110405', '1382757051,1382853177,1393110372,1393557922', '1393130283,1393131059', '6000000', '999', 'JAMSOSTEK', '20000000', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2014-05-24', '55000000', '6000000', '15', '200000', 'trial', '2014-06-12', 'admin', '0000-00-00', '', 'admin', '132', '0', '0000-00-00', '0', '0', '0', null, '', '66', 'admin', '1401953915');
INSERT INTO `t_penjualan2` VALUES ('1402033782', '1402033674', '02', '021', '1393089671', '13930896809', '1398474097', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2014-06-06', '', '', '', '', 'Mengundurkan diri', '0000-00-00', 'admin', '0000-00-00', null, null, '131', '1', '2014-06-12', '5000000', '1', '1000000', 'Mengundurkan diri', null, '0', 'admin', '1402033782');

-- ----------------------------
-- Table structure for `t_pilih_agama`
-- ----------------------------
DROP TABLE IF EXISTS `t_pilih_agama`;
CREATE TABLE `t_pilih_agama` (
  `id` char(10) NOT NULL,
  `agama` char(10) DEFAULT NULL,
  `urutan` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_pilih_agama
-- ----------------------------
INSERT INTO `t_pilih_agama` VALUES ('budha', 'Budha', '5');
INSERT INTO `t_pilih_agama` VALUES ('hindu', 'Hindu', '4');
INSERT INTO `t_pilih_agama` VALUES ('islam', 'Islam', '1');
INSERT INTO `t_pilih_agama` VALUES ('katolik', 'Katolik', '3');
INSERT INTO `t_pilih_agama` VALUES ('kristen', 'Kristen', '2');

-- ----------------------------
-- Table structure for `t_potongan`
-- ----------------------------
DROP TABLE IF EXISTS `t_potongan`;
CREATE TABLE `t_potongan` (
  `id_potongan` char(15) NOT NULL,
  `id_penjualan` char(15) DEFAULT NULL,
  `nama_potongan` char(50) DEFAULT NULL,
  `jml_potongan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_potongan`),
  KEY `id_penjualan` (`id_penjualan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_potongan
-- ----------------------------
INSERT INTO `t_potongan` VALUES ('1398751746', '1398542286', 'POT DEP KEU', '175820');
INSERT INTO `t_potongan` VALUES ('1398751772', '1398542286', 'PAJAK PENGHASILAN', '37737');

-- ----------------------------
-- Table structure for `t_proyek`
-- ----------------------------
DROP TABLE IF EXISTS `t_proyek`;
CREATE TABLE `t_proyek` (
  `id_proyek` int(11) NOT NULL,
  `nama_proyek` varchar(100) DEFAULT NULL,
  `alamat_proyek` text,
  `publish` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_proyek`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_proyek
-- ----------------------------
INSERT INTO `t_proyek` VALUES ('1393089671', 'VILLA DELIA UNGARAN', '-', '1');
INSERT INTO `t_proyek` VALUES ('1393090522', 'GRIYA PRIMA ONGGORAWE', '-', '1');

-- ----------------------------
-- Table structure for `t_rab`
-- ----------------------------
DROP TABLE IF EXISTS `t_rab`;
CREATE TABLE `t_rab` (
  `id_rab` char(15) NOT NULL,
  `id_proyek` char(15) NOT NULL,
  `tipe` char(15) NOT NULL,
  `unit` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_rab`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_rab
-- ----------------------------
INSERT INTO `t_rab` VALUES ('1403166846', '1393090522', '36', '50');
INSERT INTO `t_rab` VALUES ('1405299871', '', '36', '200');

-- ----------------------------
-- Table structure for `t_rab_detail`
-- ----------------------------
DROP TABLE IF EXISTS `t_rab_detail`;
CREATE TABLE `t_rab_detail` (
  `id` int(11) NOT NULL,
  `rab_id` int(11) NOT NULL,
  `jenis_uraian` varchar(255) NOT NULL,
  `uraian` varchar(255) NOT NULL,
  `satuan` varchar(255) NOT NULL,
  `volume` float NOT NULL DEFAULT '0',
  `bobot` float NOT NULL DEFAULT '0',
  `harga_satuan` bigint(20) NOT NULL DEFAULT '0',
  `jumlah_harga` bigint(20) NOT NULL DEFAULT '0',
  `jumlah_harga_50unit` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_rab_detail
-- ----------------------------
INSERT INTO `t_rab_detail` VALUES ('1405606155', '1403166846', 'TENAGA', 'Pekerjaan Batu (*)', 'Ls', '1', '96.4769', '125000000', '125000000', '6250000000');
INSERT INTO `t_rab_detail` VALUES ('1405636783', '1403166846', 'MATERIAL/BAHAN', 'Baja Ringan Profil C', 'BTG', '16', '0.555707', '45000', '720000', '36000000');
INSERT INTO `t_rab_detail` VALUES ('1405636809', '1403166846', 'MATERIAL/BAHAN', 'Baja Ringan Profil Reng', 'BTG', '22', '1.61309', '95000', '2090000', '104500000');
INSERT INTO `t_rab_detail` VALUES ('1405637730', '1403166846', 'TENAGA', 'Pekerjaan Atap', 'M<sup>2</sup>', '70.19', '1.35434', '25000', '1754750', '87737500');

-- ----------------------------
-- Table structure for `t_rab_jenis_uraian`
-- ----------------------------
DROP TABLE IF EXISTS `t_rab_jenis_uraian`;
CREATE TABLE `t_rab_jenis_uraian` (
  `jenis_uraian` varchar(255) NOT NULL,
  PRIMARY KEY (`jenis_uraian`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_rab_jenis_uraian
-- ----------------------------
INSERT INTO `t_rab_jenis_uraian` VALUES ('MATERIAL/BAHAN');
INSERT INTO `t_rab_jenis_uraian` VALUES ('TENAGA');

-- ----------------------------
-- Table structure for `t_satuan`
-- ----------------------------
DROP TABLE IF EXISTS `t_satuan`;
CREATE TABLE `t_satuan` (
  `satuan` varchar(255) NOT NULL,
  PRIMARY KEY (`satuan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_satuan
-- ----------------------------
INSERT INTO `t_satuan` VALUES ('BTG');
INSERT INTO `t_satuan` VALUES ('Liter');
INSERT INTO `t_satuan` VALUES ('Ls');
INSERT INTO `t_satuan` VALUES ('M<sup>2</sup>');
INSERT INTO `t_satuan` VALUES ('M<sup>3</sup>');
INSERT INTO `t_satuan` VALUES ('Reit');

-- ----------------------------
-- Table structure for `t_setting`
-- ----------------------------
DROP TABLE IF EXISTS `t_setting`;
CREATE TABLE `t_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_setting
-- ----------------------------
INSERT INTO `t_setting` VALUES ('1', '');

-- ----------------------------
-- Table structure for `t_status_hubungan`
-- ----------------------------
DROP TABLE IF EXISTS `t_status_hubungan`;
CREATE TABLE `t_status_hubungan` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `kode` char(5) DEFAULT NULL,
  `keterangan` char(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_status_hubungan
-- ----------------------------
INSERT INTO `t_status_hubungan` VALUES ('1', '4', 'ORANG TUA');
INSERT INTO `t_status_hubungan` VALUES ('2', '1', 'SAUDARA KANDUNG');
INSERT INTO `t_status_hubungan` VALUES ('3', '2', 'ANAK');
INSERT INTO `t_status_hubungan` VALUES ('4', '3', 'SAUDARA KANDUNG ORANG TUA');
INSERT INTO `t_status_hubungan` VALUES ('5', '99', 'LAINNYA .....');
INSERT INTO `t_status_hubungan` VALUES ('6', '0', 'KOSONG / TIDAK ADA');

-- ----------------------------
-- Table structure for `t_status_keluarga`
-- ----------------------------
DROP TABLE IF EXISTS `t_status_keluarga`;
CREATE TABLE `t_status_keluarga` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `kode` char(5) DEFAULT NULL,
  `keterangan` char(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_status_keluarga
-- ----------------------------
INSERT INTO `t_status_keluarga` VALUES ('1', '0', 'Ayah / Bapak');
INSERT INTO `t_status_keluarga` VALUES ('2', '1', 'Ibu');
INSERT INTO `t_status_keluarga` VALUES ('3', '2', 'Suami');
INSERT INTO `t_status_keluarga` VALUES ('4', '3', 'Istri');
INSERT INTO `t_status_keluarga` VALUES ('5', '4', 'Anak');
INSERT INTO `t_status_keluarga` VALUES ('6', '5', 'Saudara');

-- ----------------------------
-- Table structure for `t_status_pekerjaan_pasangan`
-- ----------------------------
DROP TABLE IF EXISTS `t_status_pekerjaan_pasangan`;
CREATE TABLE `t_status_pekerjaan_pasangan` (
  `id` tinyint(1) NOT NULL AUTO_INCREMENT,
  `kode` char(1) DEFAULT NULL,
  `keterangan` char(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_status_pekerjaan_pasangan
-- ----------------------------
INSERT INTO `t_status_pekerjaan_pasangan` VALUES ('1', '0', 'TIDAK BEKERJA');
INSERT INTO `t_status_pekerjaan_pasangan` VALUES ('2', '1', 'BEKERJA');

-- ----------------------------
-- Table structure for `t_status_pernikahan`
-- ----------------------------
DROP TABLE IF EXISTS `t_status_pernikahan`;
CREATE TABLE `t_status_pernikahan` (
  `id` tinyint(1) NOT NULL AUTO_INCREMENT,
  `kode` char(1) DEFAULT NULL,
  `keterangan` char(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_status_pernikahan
-- ----------------------------
INSERT INTO `t_status_pernikahan` VALUES ('1', '0', 'LAJANG');
INSERT INTO `t_status_pernikahan` VALUES ('2', '1', 'MENIKAH');
INSERT INTO `t_status_pernikahan` VALUES ('3', '2', 'DUDA / JANDA');

-- ----------------------------
-- Table structure for `t_status_proses`
-- ----------------------------
DROP TABLE IF EXISTS `t_status_proses`;
CREATE TABLE `t_status_proses` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `kode` char(5) DEFAULT NULL,
  `keterangan` char(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_status_proses
-- ----------------------------
INSERT INTO `t_status_proses` VALUES ('1', '131', 'PRAPENJUALAN');
INSERT INTO `t_status_proses` VALUES ('2', '132', 'ACC KREDIT');
INSERT INTO `t_status_proses` VALUES ('3', '133', 'AKAD KREDIT / AJB');

-- ----------------------------
-- Table structure for `t_status_rumah`
-- ----------------------------
DROP TABLE IF EXISTS `t_status_rumah`;
CREATE TABLE `t_status_rumah` (
  `id` tinyint(1) NOT NULL AUTO_INCREMENT,
  `kode` char(1) DEFAULT NULL,
  `keterangan` char(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_status_rumah
-- ----------------------------
INSERT INTO `t_status_rumah` VALUES ('1', '1', 'MILIK SENDIRI');
INSERT INTO `t_status_rumah` VALUES ('2', '2', 'KONTRAK / SEWA');
INSERT INTO `t_status_rumah` VALUES ('3', '3', 'KELUARGA');
INSERT INTO `t_status_rumah` VALUES ('4', '4', 'DINAS');

-- ----------------------------
-- Table structure for `t_sts_marketing`
-- ----------------------------
DROP TABLE IF EXISTS `t_sts_marketing`;
CREATE TABLE `t_sts_marketing` (
  `idsts` int(11) NOT NULL,
  `nama_status` char(25) NOT NULL,
  `publish` smallint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idsts`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_sts_marketing
-- ----------------------------
INSERT INTO `t_sts_marketing` VALUES ('1397913214', 'KARYAWAN', '1');
INSERT INTO `t_sts_marketing` VALUES ('1397913230', 'FREELANCE', '1');
INSERT INTO `t_sts_marketing` VALUES ('1399273426', 'YKPP', '1');
INSERT INTO `t_sts_marketing` VALUES ('1399273523', 'DIAN', '1');

-- ----------------------------
-- Table structure for `t_supplier`
-- ----------------------------
DROP TABLE IF EXISTS `t_supplier`;
CREATE TABLE `t_supplier` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `hp` varchar(255) NOT NULL,
  `ktp` varchar(255) NOT NULL,
  `toko` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_supplier
-- ----------------------------

-- ----------------------------
-- Table structure for `t_syarat_bank`
-- ----------------------------
DROP TABLE IF EXISTS `t_syarat_bank`;
CREATE TABLE `t_syarat_bank` (
  `id_syarat` char(15) NOT NULL,
  `id_bank` char(15) DEFAULT NULL,
  `nama_persyaratan` varchar(100) DEFAULT NULL,
  `urutan` int(5) DEFAULT '0',
  `publish` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_syarat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_syarat_bank
-- ----------------------------
INSERT INTO `t_syarat_bank` VALUES ('1393130283', '1393110405', 'Permohonan Kredit', '1', '1');
INSERT INTO `t_syarat_bank` VALUES ('1393131059', '1393110405', 'I-2 ( Developer )', '2', '1');
INSERT INTO `t_syarat_bank` VALUES ('1393131095', '1393110405', '1-1 ( p. penghasilan tetap )', '3', '1');
INSERT INTO `t_syarat_bank` VALUES ('1393131191', '1393110405', 'Ket. Gaji / Slip Gaji', '4', '1');
INSERT INTO `t_syarat_bank` VALUES ('1393131481', '1393110405', 'E   SURAT PERNYATAAN PENGHASILAN', '5', '1');
INSERT INTO `t_syarat_bank` VALUES ('1393131603', '1393110405', 'F SURAT KET.BLM MEMILIKI RUMAH', '6', '1');
INSERT INTO `t_syarat_bank` VALUES ('1393131736', '1393110405', 'SK Pegawai', '7', '1');
INSERT INTO `t_syarat_bank` VALUES ('1393131847', '1393110405', 'Spt Tahunan', '8', '1');
INSERT INTO `t_syarat_bank` VALUES ('1393558066', '1393110405', 'Kuasa Potong Gaji', '9', '1');
INSERT INTO `t_syarat_bank` VALUES ('1393558086', '1393110405', 'G SRT PERNYTN PEMOHON KPR SEJAHTERA', '10', '1');
INSERT INTO `t_syarat_bank` VALUES ('1393558091', '1393110405', 'Fc. Srtifikat', '12', '1');
INSERT INTO `t_syarat_bank` VALUES ('1393558095', '1393110405', 'FC. IMB', '11', '1');
INSERT INTO `t_syarat_bank` VALUES ('1399871370', '1393110405', 'SK BEKERJA PASANGAN', '13', '1');
INSERT INTO `t_syarat_bank` VALUES ('1399871401', '1393110405', 'SLIP GAJI/KET.PENGHASILAN PASANGAN', '14', '1');
INSERT INTO `t_syarat_bank` VALUES ('1399871466', '1393110405', 'FC NPWP PEMOHON', '15', '1');
INSERT INTO `t_syarat_bank` VALUES ('1400038710', '1393110405', 'fc. kartu jamsostek', '16', '1');

-- ----------------------------
-- Table structure for `t_syarat_personal`
-- ----------------------------
DROP TABLE IF EXISTS `t_syarat_personal`;
CREATE TABLE `t_syarat_personal` (
  `id_syarat` char(15) NOT NULL,
  `nama_syarat` varchar(50) DEFAULT NULL,
  `urutan` int(5) DEFAULT '0',
  `publish` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id_syarat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_syarat_personal
-- ----------------------------
INSERT INTO `t_syarat_personal` VALUES ('1382757051', 'Pas Foto, suami, istri', '1', '1');
INSERT INTO `t_syarat_personal` VALUES ('1382853177', 'Fc. KTP , suami, istri', '2', '1');
INSERT INTO `t_syarat_personal` VALUES ('1393110354', 'Fc. NPWP', '12', '1');
INSERT INTO `t_syarat_personal` VALUES ('1393110372', 'Fc. Kartu Keluarga', '3', '1');
INSERT INTO `t_syarat_personal` VALUES ('1393110388', 'Fc. Buku Tabungan', '11', '1');
INSERT INTO `t_syarat_personal` VALUES ('1393321618', 'Fc. Surat Nikah', '10', '1');
INSERT INTO `t_syarat_personal` VALUES ('1393557899', 'Pas Poto Pemohon', '7', '1');
INSERT INTO `t_syarat_personal` VALUES ('1393557922', 'Fc. Buku Tabungan 3 bln Terakhir', '5', '1');
INSERT INTO `t_syarat_personal` VALUES ('1393557943', 'Fc. Akta Cerai', '6', '1');
INSERT INTO `t_syarat_personal` VALUES ('1393557959', 'SK/ Srt ket Bekerja', '8', '1');
INSERT INTO `t_syarat_personal` VALUES ('1393558018', 'SPT Tahunan', '9', '1');

-- ----------------------------
-- Table structure for `t_tipe_kas`
-- ----------------------------
DROP TABLE IF EXISTS `t_tipe_kas`;
CREATE TABLE `t_tipe_kas` (
  `id` smallint(5) NOT NULL AUTO_INCREMENT,
  `tipekas` char(25) NOT NULL,
  `showsaldo` smallint(1) NOT NULL DEFAULT '1',
  `aktif` smallint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_tipe_kas
-- ----------------------------
INSERT INTO `t_tipe_kas` VALUES ('1', 'Kas/Bank', '1', '1');
INSERT INTO `t_tipe_kas` VALUES ('2', 'Akun Piutang', '0', '1');
INSERT INTO `t_tipe_kas` VALUES ('3', 'Persediaan', '1', '1');
INSERT INTO `t_tipe_kas` VALUES ('4', 'Aktiva Lancar Lainnya', '1', '1');
INSERT INTO `t_tipe_kas` VALUES ('5', 'Aktiva Tetap', '0', '1');
INSERT INTO `t_tipe_kas` VALUES ('6', 'Akumulasi Penyusutan', '0', '1');
INSERT INTO `t_tipe_kas` VALUES ('7', 'Aktiva Lainnya', '1', '1');
INSERT INTO `t_tipe_kas` VALUES ('8', 'Akun Hutang', '0', '1');
INSERT INTO `t_tipe_kas` VALUES ('9', 'Hutang Lancar Lainnya', '1', '1');
INSERT INTO `t_tipe_kas` VALUES ('10', 'Hutang Jangka Panjang', '1', '1');
INSERT INTO `t_tipe_kas` VALUES ('11', 'Ekuitas', '1', '1');
INSERT INTO `t_tipe_kas` VALUES ('12', 'Pendapatan', '1', '1');
INSERT INTO `t_tipe_kas` VALUES ('13', 'Harga Pokok Penjualan', '1', '1');
INSERT INTO `t_tipe_kas` VALUES ('14', 'Beban', '1', '1');
INSERT INTO `t_tipe_kas` VALUES ('15', 'Beban Lain-Lain', '1', '1');
INSERT INTO `t_tipe_kas` VALUES ('16', 'Pendapatan Lainnya', '1', '1');

-- ----------------------------
-- Table structure for `t_topmenu`
-- ----------------------------
DROP TABLE IF EXISTS `t_topmenu`;
CREATE TABLE `t_topmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `module` varchar(100) NOT NULL,
  `menu` char(20) DEFAULT NULL,
  `url` varchar(50) DEFAULT NULL,
  `urutan` smallint(6) DEFAULT NULL,
  `aktif` smallint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_topmenu
-- ----------------------------
INSERT INTO `t_topmenu` VALUES ('1', 'Input Data', 'input', 'user', null, '1', '1');

-- ----------------------------
-- Table structure for `t_yes_no`
-- ----------------------------
DROP TABLE IF EXISTS `t_yes_no`;
CREATE TABLE `t_yes_no` (
  `id` tinyint(1) NOT NULL AUTO_INCREMENT,
  `kode` char(1) DEFAULT NULL,
  `keterangan` char(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_yes_no
-- ----------------------------
INSERT INTO `t_yes_no` VALUES ('1', '0', 'Tidak');
INSERT INTO `t_yes_no` VALUES ('2', '1', 'Ya');
