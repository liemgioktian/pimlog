<?php

class MY_Model extends CI_Model {

    private $table;
    private $primary_key;
    
    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function setTable($table){
        $this->table = $table;
    }
    
    function setPrimaryKey($field){
        $this->primary_key = $field;
    }
    
    function getAll(){
        return $this->db->get($this->table)->result();
    }
    
    function findAll($where){
        return $this->db->get_where($this->table, $where)->result();
    }
    
    function find($where){
        return $this->db->get_where($this->table, $where)->row_array();
    }
    
    function add($data){
        return $this->db->insert($this->table,$data);
    }
    
    function update($data){
        return $this->db->where($this->primary_key, $data[$this->primary_key])->update($this->table,$data);
    }
    
    function delete($primary_key){
        return $this->db->where($this->primary_key,$primary_key)->delete($this->table);
    }
}