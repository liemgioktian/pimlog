<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class t_satuan extends MY_Model{
    
    public $table = 't_satuan';
    function __construct() {
        parent::__construct();
        parent::setTable($this->table);
        parent::setPrimaryKey('satuan');
    }
    
    function validate_add($data){
        if(strlen($data['satuan'])<1) return 'isian kurang lengkap';
        if(parent::add ($data)) return true;
        else return 'proses gagal';
    }
    
    function validate_update($data){
        if(strlen($data['satuan'])<1) return 'isian kurang lengkap';
        if(parent::update ($data)) return true;
        else return 'proses gagal';
    }
}