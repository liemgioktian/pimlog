<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class t_rab extends MY_Model{
    
    public $table = 't_rab';
    function __construct() {
        parent::__construct();
        parent::setTable($this->table);
    }
    
    function getAll() {
        return $this->db
                ->select('t_proyek.nama_proyek')
                ->select('t_rab.*')
                ->from('t_rab')
                ->join('t_proyek','t_rab.id_proyek=t_proyek.id_proyek','left')
                ->get()
                ->result();
    }
    
    function find($rab){
        return $this->db
                ->select('t_proyek.nama_proyek')
                ->select('t_rab.*')
                ->from('t_rab')
                ->join('t_proyek','t_rab.id_proyek=t_proyek.id_proyek','left')
                ->get()
                ->row_array();
    }
}