<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class t_rab_detail extends MY_Model{
    
    public $table = 't_rab_detail';
    public $primary_key = 'id';
    function __construct() {
        parent::__construct();
        parent::setTable($this->table);
        parent::setPrimaryKey($this->primary_key);
    }
    
    function validate_add($data){
        return $this->presave($data);
    }
    
    function presave($data){
//        $data['bobot'] = $data['jumlah_harga']/$H$80*100
        $data['jumlah_harga'] = $data['volume'] * $data['harga_satuan'];
        $data['jumlah_harga_50unit'] = $data['jumlah_harga'] * 50;
        if(parent::add($data)) $this->postupdate ($data['rab_id']);
        else return 'proses gagal';
    }
    
    function delete($primary_key){
        $uraian = parent::find(array($this->primary_key=>$primary_key));
        $rab_id = $uraian['rab_id'];
        if($this->db->where($this->primary_key,$primary_key)->delete($this->table))
            return $this->postupdate ($rab_id);
    }
    
    function postupdate($rab_id){
        $sums = $this->getSums($rab_id);
        foreach (parent::findAll(array('rab_id'=>$rab_id)) as $detail) {
            $detail = (array) $detail;
            $detail['bobot'] = $detail['jumlah_harga'] / $sums['jumlah_harga'] * 100;
            parent::update($detail);
        }
    }
    
    function getDistinctJenisUraian($rab_id){
        $this->db->distinct();
        return $this->db
                ->select('jenis_uraian')
                ->from($this->table)
                ->where('rab_id',$rab_id)
                ->get()->result();
    }
    
    function getSums($rab_id){
        $this->db->where('rab_id',$rab_id);
        $this->db
                ->select_sum('bobot')
                ->select_sum('jumlah_harga')
                ->select_sum('jumlah_harga_50unit');
        return $this->db->get($this->table)->row_array();
    }
}