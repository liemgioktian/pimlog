<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class t_supplier extends MY_Model{
    
    public $table = 't_supplier';
    function __construct() {
        parent::__construct();
        parent::setTable($this->table);
        parent::setPrimaryKey('id');
    }
    
    function validate_add($data){
        if(strlen($data['nama'])<1) return 'isian kurang lengkap';
        if(parent::add ($data)) return true;
        else return 'proses gagal';
    }
    
    function validate_update($data){
        if(strlen($data['nama'])<1) return 'isian kurang lengkap';
        if(parent::update ($data)) return true;
        else return 'proses gagal';
    }
}