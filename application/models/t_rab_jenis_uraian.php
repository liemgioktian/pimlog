<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class t_rab_jenis_uraian extends MY_Model{
    
    public $table = 't_rab_jenis_uraian';
    function __construct() {
        parent::__construct();
        parent::setTable($this->table);
    }
    
    function validate_add($data){
        if(strlen($data['jenis_uraian'])<1) return 'isian kurang lengkap';
        if(parent::add ($data)) return true;
        else return 'proses gagal';
    }
    
}