<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-user"></i>
        <h3>Formulir Mandor</h3>
    </div>
    <div class="widget-content">
        <br/>
        <form id="edit-profile" class="form-horizontal" method="POST">
            <fieldset>
                <div class="control-group">											
                    <label class="control-label" for="username">Nama Mandor</label>
                    <div class="controls">
                        <input type="text" class="span6" name="nama" value="<?= isset($prefill)?$prefill['nama']:'' ?>">
                        <p class="help-block">masukkan nama lengkap mandor, karena sistem dapat menyimpan nama yang sama</p>
                    </div>
                </div>
                <div class="control-group">											
                    <label class="control-label" for="username">No. HP/Kontak</label>
                    <div class="controls">
                        <input type="text" class="span6" name="hp" value="<?= isset($prefill)?$prefill['hp']:'' ?>">
                    </div>
                </div>
                <div class="control-group">											
                    <label class="control-label" for="username">No. KTP/Identitas</label>
                    <div class="controls">
                        <input type="text" class="span6" name="ktp" value="<?= isset($prefill)?$prefill['ktp']:'' ?>">
                    </div>
                </div>
                <div class="control-group">											
                    <label class="control-label" for="username"></label>
                    <div class="controls">
                        <input type="submit" class="btn btn-primary" value="simpan" />
                        <a href="<?= site_url('main/mandor') ?>" class="btn btn-warning">batal</a>
                    </div>
                </div>
            </fieldset>
        </form>

    </div>
</div>