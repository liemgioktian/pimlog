<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>
        <h3>Daftar Mandor</h3>
        <a href="<?= site_url('main/mandorbaru') ?>" class="btn pull-right btn-primary">Mandor Baru</a>
    </div>
    <div class="widget-content">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th width="10%"> ID </th>
                    <th width="20%"> Nama </th>
                    <th> No. HP/Kontak </th>
                    <th> No. KTP/Identitas </th>
                    <th width="10%"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($mandors as $mandor): ?>
                <tr>
                    <td> <?= $mandor->id ?> </td>
                    <td> <?= $mandor->nama ?> </td>
                    <td> <?= $mandor->hp ?> </td>
                    <td> <?= $mandor->ktp ?> </td>
                    <td>
                        <div class="btn-group">
                            <form method="POST">
                                <a href="<?= site_url('main/mandoredit/'.$mandor->id) ?>" class="btn btn-small btn-warning">edit</a>
                                <input type="hidden" value="<?= $mandor->id ?>" name="id" />
                                <input type="submit" class="btn btn-small btn-danger confirm" value="hapus" />
                            </form>
                        </div>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>