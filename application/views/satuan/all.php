<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>
        <h3>Daftar Satuan RAB</h3>
    </div>
    <div class="widget-content">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th> Satuan </th>
                    <th width="20%"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($satuans as $satuan): ?>
                <tr>
                    <form method="POST">
                    <td> <?= $satuan->satuan ?> </td>
                    <td>
                            <input type="hidden" value="<?= $satuan->satuan ?>" name="satuan" />
                            <input type="submit" name="delete" class="btn btn-small btn-danger confirm" value="hapus" />
                    </td>
                    </form>
                </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
            <form method="POST">
                <tr>
                    <th>
                    <div class="input-append">
                        <input type="text" name="satuan" />
                        <input type="submit" value="simpan" class="btn btn-primary" name="add" />
                    </div>
                    </th>
                    <th></th>
                </tr>
                </form>
            </tfoot>
        </table>
    </div>
</div>