<div class="widget widget-table action-table">
    <div class="widget-header"> 
        <i class="icon-th-list"></i>
        <h3>Detail RAB <?= $rab['id_rab'].' PROYEK '.$rab['nama_proyek'] ?></h3>
        
        <div class="btn-group pull-right" style="margin-left: 10px">
            <a class="btn btn-info marright0" href="<?= site_url("main/rab") ?>"><i class="icon icon-circle-arrow-left marleft0"></i></a>
            <a href="" class="btn btn-warning marright0"><i class="icon icon-print marleft0"></i></a>
            <a href="" class="btn btn-warning marright0"><i class="icon icon-download-alt marleft0"></i></a>
            <a class="btn btn-info" href="<?= current_url() ?>"><i class="icon icon-refresh marleft0"></i></a>
        </div>
        
        <div class="pull-right input-append">
            <form method="POST" class="marbot0">
                <input type="text" name="jenis_uraian" placeholder="tambah jenis uraian.." class="span2" />
                <input type="submit" value="+" class="btn btn-primary" name="add_jenis_uraian">
            </form>
        </div>
        
        
    </div>
    <div class="widget-content">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th width="5%">NO</th>
                    <th>URAIAN</th>
                    <th width="6%">SATUAN</th>
                    <th width="6%">VOLUME</th>
                    <th width="6%">BOBOT</th>
                    <th width="12%">HARGA SATUAN</th>
                    <th width="12%">JUMLAH HARGA</th>
                    <th width="12%">JUMLAH HARGA 50 Unit</th>
                    <th width="5%"></th>
                </tr>
            </thead>
            <tbody>
                <?php $jenurno='A'; foreach ($grouping_row as $group): ?>
                <tr class="txtbold">
                    <td class="txtalrt"><?= $jenurno ?></td>
                    <td colspan="8"><?= $group->jenis_uraian ?></td>
                </tr>
                    <?php $no=0; foreach($details as $detail): if($detail->jenis_uraian===$group->jenis_uraian): $no++; ?>
                    <tr>
                        <form method="POST">
                            <td class="txtalrt"><?= $no; ?></td>
                            <td> <?= $detail->uraian ?> </td>
                            <td class=""> <?= $detail->satuan ?> </td>
                            <td class="txtalrt"> <?= $detail->volume ?> </td>
                            <td class="txtalrt"> <?= round($detail->bobot, 2) ?> </td>
                            <td class="txtalrt"> Rp <?= number_format($detail->harga_satuan,0,",",".") ?> </td>
                            <td class="txtalrt"> Rp <?= number_format($detail->jumlah_harga,0,",",".") ?> </td>
                            <td class="txtalrt"> Rp <?= number_format($detail->jumlah_harga_50unit,0,",",".") ?> </td>
                            <td>
                                <input type="hidden" value="<?= $detail->id ?>" name="id" />
                                <input type="submit" name="delete_uraian" class="btn btn-small btn-danger confirm" value="hapus" />
                            </td>
                        </form>
                    </tr>
                    <?php endif; endforeach; ?>
                <?php $jenurno++; endforeach; ?>
            </tbody>
            <tfoot>
                <tr class="txtbold">
                    <td></td>
                    <td colspan="3">Total</td>
                    <td class="txtalrt"><?= round($sums['bobot'], 2) ?></td>
                    <td></td>
                    <td class="txtalrt">Rp <?= number_format($sums['jumlah_harga'],0,",",".") ?></td>
                    <td class="txtalrt">Rp <?= number_format($sums['jumlah_harga_50unit'],0,",",".") ?></td>
                    <td></td>
                </tr>
                <tr>
                <form method="POST">
                    <th></th>
                    <th colspan="1">
                        <div class="input-append">
                            <input type="text" name="uraian" class="span2" />
                            <select name="jenis_uraian" class="span2 marbot0">
                                <?php foreach($jenis_uraians as $jenis_uraian): ?>
                                <option value="<?= $jenis_uraian->jenis_uraian ?>"><?= $jenis_uraian->jenis_uraian ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </th>
                    <th>
                        <select name="satuan" class="span2">
                            <?php foreach($satuans as $satuan): ?>
                            <option value="<?= $satuan->satuan ?>"><?= $satuan->satuan ?></option>
                            <?php endforeach; ?>
                        </select>
                    </th>
                    <th>
                        <input type="text" name="volume" class="span1" />
                    </th>
                    <th>
                        <!--<input type="text" name="bobot" class="span1" />-->
                    </th>
                    <th class="txtalrt">
                        <input type="text" name="harga_satuan" class="span1" />
                    </th>
                    <th>
                        <!--<input type="text" name="jumlah_harga" class="span1" />-->
                    </th>
                    <th>
                        <!--<input type="text" name="jumlah_harga_50unit" class="span1" />-->
                    </th>
                    <th>
                        <input type="submit" name="tambah_uraian" value="simpan" class="btn btn-small btn-primary" />
                    </th>
                </form>
                </tr>
            </tfoot>
        </table>
    </div>
</div>