<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>
        <h3>Daftar RAB</h3>
    </div>
    <div class="widget-content">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th> ID </th>
                    <th> Proyek </th>
                    <th> Type </th>
                    <th> Unit </th>
                    <th width="10%"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($rabs as $rab): ?>
                <tr>
                    <td> <?= $rab->id_rab ?> </td>
                    <td> <?= $rab->nama_proyek ?> </td>
                    <td> <?= $rab->tipe ?> </td>
                    <td> <?= $rab->unit ?> </td>
                    <td><a href="<?= site_url("main/rab_detail/$rab->id_rab") ?>" class="btn btn-small btn-primary">detail</a></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>