<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>
        <h3>Daftar Proyek Aktif</h3>
    </div>
    <!-- /widget-header -->
    <div class="widget-content">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th> ID </th>
                    <th> Proyek </th>
                    <th> Alamat </th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($proyeks as $proyek): ?>
                <tr>
                    <td> <?= $proyek->id_proyek ?> </td>
                    <td> <?= $proyek->nama_proyek ?> </td>
                    <td> <?= $proyek->alamat_proyek ?> </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>