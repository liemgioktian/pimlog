<?php if(!is_null($msg)): ?>
<div class="widget">
    <div class="alert">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Perhatian!</strong> <?= $msg ?>
    </div>
</div>
<?php endif; ?>