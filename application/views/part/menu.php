<div class="subnavbar">
    <div class="subnavbar-inner">
        <div class="container">
            <ul class="mainnav">
                <?php $menu_active = array('','','','','','',''); if(isset($menu)) $menu_active[$menu]='active'; ?>
                <li class="<?= $menu_active[1] ?>"><a href="<?= site_url() ?>"><i class="icon-dashboard"></i><span>Proyek</span> </a> </li>
                <li class="<?= $menu_active[2] ?>"><a href="<?= site_url('main/rab') ?>"><i class="icon-list-alt"></i><span>RAB</span> </a> </li>
                <li class="dropdown <?= $menu_active[3] ?>">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> 
                        <i class="icon-shopping-cart"></i>
                        <span>Belanja</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:;">FPU (form permintaan uang)</a></li>
                        <li><a href="javascript:;">FPB (form penerimaan barang)</a></li>
                        <li><a href="javascript:;">Buku Jatah</a></li>
                    </ul>
                </li>
                <li class="<?= $menu_active[4] ?>"><a href="<?= site_url('main/mandor') ?>"><i class="icon-user"></i><span>Mandor</span> </a> </li>
                <li class="<?= $menu_active[6] ?>"><a href="<?= site_url('main/supplier') ?>"><i class="icon-truck"></i><span>Supplier</span> </a> </li>
                <li class="dropdown <?= $menu_active[5] ?>">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> 
                        <i class="icon-cog"></i>
                        <span>Pengaturan</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= site_url('main/satuan') ?>">satuan item RAB</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>