<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>PT.PIM | Logistic</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('assets/css/bootstrap-responsive.min.css') ?>" rel="stylesheet">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
        <link href="<?= base_url('assets/css/font-awesome.css') ?>" rel="stylesheet">
        <link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet">
        <link href="<?= base_url('assets/css/pimlog.css') ?>" rel="stylesheet">
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
              <script src="http://html5shim.googlecode.com/svn/trunk/html5.js') ?>"></script>
        <![endif]-->
    </head>
    <body>
        
        <?php if(!isset($msg)) $msg = ($this->session->flashdata('msg'))?$this->session->flashdata('msg'):null; ?>
        
        <?php include APPPATH.'/views/part/header.php'; ?>
        <?php if($this->session->userdata('iduser')): ?>
            <?php include APPPATH.'/views/part/menu.php'; ?>
            <div class="main">
                <div class="main-inner">
                    <div class="container">
                        <div class="row">

                            <?php include APPPATH.'/views/part/notification.php'; ?>
                            <?php include isset($viewer)?APPPATH.'/views/'.$viewer.'.php':APPPATH.'/views/dashboard.php'; ?>

                        </div>
                    </div>
                </div>
            </div>
            <?php include APPPATH.'/views/part/extra.php'; ?>
            <?php include APPPATH.'/views/part/footer.php'; ?>
        <?php else: ?>
            <link href="<?= base_url('assets/css/pages/signin.css') ?>" rel="stylesheet">
            <?php include APPPATH.'/views/login.php'; ?>
        <?php endif; ?>
        

        <script src="<?= base_url('assets/js/jquery-1.7.2.min.js') ?>"></script> 
        <script src="<?= base_url('assets/js/bootstrap.js') ?>"></script>
        <script src="<?= base_url('assets/js/base.js') ?>"></script> 
    </body>
</html>