<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>
        <h3>Daftar Supplier</h3>
        <a href="<?= site_url('main/supplierbaru') ?>" class="btn pull-right btn-primary">Supplier Baru</a>
    </div>
    <div class="widget-content">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th width="10%"> ID </th>
                    <th width="20%"> Nama </th>
                    <th> No. HP/Kontak </th>
                    <th> No. KTP/Identitas </th>
                    <th width="30%"> Alamat Toko </th>
                    <th width="10%"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($suppliers as $supplier): ?>
                <tr>
                    <td> <?= $supplier->id ?> </td>
                    <td> <?= $supplier->nama ?> </td>
                    <td> <?= $supplier->hp ?> </td>
                    <td> <?= $supplier->ktp ?> </td>
                    <td> <?= $supplier->toko ?> </td>
                    <td>
                        <div class="btn-group">
                            <form method="POST">
                                <a href="<?= site_url('main/supplieredit/'.$supplier->id) ?>" class="btn btn-small btn-warning">edit</a>
                                <input type="hidden" value="<?= $supplier->id ?>" name="id" />
                                <input type="submit" class="btn btn-small btn-danger confirm" value="hapus" />
                            </form>
                        </div>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>