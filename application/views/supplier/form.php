<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-user"></i>
        <h3>Formulir Supplier</h3>
    </div>
    <div class="widget-content">
        <br/>
        <form id="edit-profile" class="form-horizontal" method="POST">
            <fieldset>
                <div class="control-group">											
                    <label class="control-label" for="username">Nama Supplier</label>
                    <div class="controls">
                        <input type="text" class="span6" name="nama" value="<?= isset($prefill)?$prefill['nama']:'' ?>">
                    </div>
                </div>
                <div class="control-group">											
                    <label class="control-label" for="username">No. HP/Kontak</label>
                    <div class="controls">
                        <input type="text" class="span6" name="hp" value="<?= isset($prefill)?$prefill['hp']:'' ?>">
                    </div>
                </div>
                <div class="control-group">											
                    <label class="control-label" for="username">No. KTP/Identitas</label>
                    <div class="controls">
                        <input type="text" class="span6" name="ktp" value="<?= isset($prefill)?$prefill['ktp']:'' ?>">
                    </div>
                </div>
                <div class="control-group">											
                    <label class="control-label" for="username">Alamat Toko</label>
                    <div class="controls">
                        <input type="text" class="span6" name="toko" value="<?= isset($prefill)?$prefill['toko']:'' ?>">
                    </div>
                </div>
                <div class="control-group">											
                    <label class="control-label" for="username"></label>
                    <div class="controls">
                        <input type="submit" class="btn btn-primary" value="simpan" />
                        <a href="<?= site_url('main/supplier') ?>" class="btn btn-warning">batal</a>
                    </div>
                </div>
            </fieldset>
        </form>

    </div>
</div>