<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Auth extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->model('t_data_pengguna');
    }
    
    function index(){
        if(!$this->session->userdata('iduser')){
            $post = $this->input->post();
            $user['userid'] = $post['username'];
            $user['passid'] = $post['password'];
            $login = $this->t_data_pengguna->find($user);
            if(empty($login)) $this->session->set_flashdata('msg','Login gagal');
            else{
                $this->session->set_userdata($login);
                $this->session->set_flashdata('msg','Login sukses');
            }
        }else{ 
            $this->session->sess_destroy();
            $this->session->set_flashdata('msg','Logout Sukses');
        }
        redirect(base_url());
    }
}