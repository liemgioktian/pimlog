<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller{
    function index(){
        $this->load->model('t_proyek');
        $data['proyeks'] = $this->t_proyek->findAll(array('publish'=>1));
        $data['viewer'] = 'proyek/tabel';
        $data['menu'] = 1;
        $this->load->view('main',$data);
    }
    
    function rab(){
        $this->load->model('t_rab');
        $data['rabs'] = $this->t_rab->getAll();
        $data['viewer'] = 'rab/tabel';
        $data['menu'] = 2;
        $this->load->view('main',$data);
    }
    
    function rab_detail($rab_id){
        $post = $this->input->post();
        
        $this->load->model('t_rab_detail');
        $this->load->model('t_satuan');
        $this->load->model('t_rab');
        $this->load->model('t_rab_jenis_uraian');
        
        if(isset($post['add_jenis_uraian'])){
            $jenis_uraian['jenis_uraian'] = $post['jenis_uraian'];
            $add_jnsurn = $this->t_rab_jenis_uraian->validate_add($jenis_uraian);
            if($add_jnsurn!==TRUE) $data['msg'] = $add_jnsurn;
        }
        if(isset($post['tambah_uraian'])){
            $rab_detail['jenis_uraian'] = $post['jenis_uraian'];
            $rab_detail['uraian'] = $post['uraian'];
            $rab_detail['satuan'] = $post['satuan'];
            $rab_detail['volume'] = $post['volume'];
            $rab_detail['harga_satuan'] = $post['harga_satuan'];
            $rab_detail['id'] = time();
            $rab_detail['rab_id'] = $rab_id;
            $add_uraian = $this->t_rab_detail->validate_add($rab_detail);
            if($add_uraian!==TRUE) $data['msg'] = $add_uraian;
        }
        if(isset($post['delete_uraian'])){
            $this->t_rab_detail->delete($post['id']);
        }
        
        $data['details'] = $this->t_rab_detail->findAll(array('rab_id'=>$rab_id));
        $data['sums'] = $this->t_rab_detail->getSums($rab_id);
        $data['satuans'] = $this->t_satuan->getAll();
        $data['jenis_uraians'] = $this->t_rab_jenis_uraian->getAll();
        $data['grouping_row'] = $this->t_rab_detail->getDistinctJenisUraian($rab_id);
        $data['viewer'] = 'rab/detail';
        $data['menu'] = 2;
        $data['rab'] = $this->t_rab->find(array('id_rab'=>$rab_id));
        $this->load->view('main',$data);
    }
    
    function mandor(){
        $this->load->model('t_mandor');
        $post = $this->input->post();
        if($post){
            $this->t_mandor->delete($post['id']);
        }
        $data['mandors'] = $this->t_mandor->getAll();
        $data['viewer'] = 'mandor/tabel';
        $data['menu'] = 4;
        $this->load->view('main',$data);
    }
    
    function mandorbaru(){
        $post = $this->input->post();
        if($post){
            $this->load->model('t_mandor');
            $mandor['id'] = time();
            $mandor['nama'] = $post['nama'];
            $mandor['hp'] = $post['hp'];
            $mandor['ktp'] = $post['ktp'];
            $result = $this->t_mandor->validate_add($mandor);
            if($result===true) redirect(site_url('main/mandor'));
            else{
                $data['msg'] = $result;
                $data['prefill'] = $post;
            }
        }
        $data['viewer'] = 'mandor/form';
        $data['menu'] = 4;
        $this->load->view('main',$data);
    }
    
    function mandoredit($id){
        $mandor['id'] = $id;
        $this->load->model('t_mandor');
        $post = $this->input->post();
        if($post){
            $mandor['nama'] = $post['nama'];
            $mandor['hp'] = $post['hp'];
            $mandor['ktp'] = $post['ktp'];
            $result = $this->t_mandor->validate_update($mandor);
            if($result===true) redirect(site_url('main/mandor'));
            else{
                $data['msg'] = $result;
                $data['prefill'] = $post;
            }
        }else $data['prefill'] = $this->t_mandor->find($mandor);
        $data['viewer'] = 'mandor/form';
        $data['menu'] = 4;
        $this->load->view('main',$data);
    }
    
    function satuan(){
        $this->load->model('t_satuan');
        $post = $this->input->post();
        if(isset($post['delete'])){
            $this->t_satuan->delete($post['satuan']);
        }else if(isset($post['add'])){
            $satuan['satuan'] = $post['satuan'];
            $result = $this->t_satuan->validate_add($satuan);
            if($result!==true){
                $data['msg'] = $result;
                $data['prefill'] = $post;
            }
        }
        $data['satuans'] = $this->t_satuan->getAll();
        $data['viewer'] = 'satuan/all';
        $data['menu'] = 5;
        $this->load->view('main',$data);
    }
    
    function supplier(){
        $this->load->model('t_supplier');
        $post = $this->input->post();
        if($post){
            $this->t_supplier->delete($post['id']);
        }
        $data['suppliers'] = $this->t_supplier->getAll();
        $data['viewer'] = 'supplier/tabel';
        $data['menu'] = 6;
        $this->load->view('main',$data);
    }
    
    function supplierbaru(){
        $post = $this->input->post();
        if($post){
            $this->load->model('t_supplier');
            $supplier['id'] = time();
            $supplier['nama'] = $post['nama'];
            $supplier['hp'] = $post['hp'];
            $supplier['ktp'] = $post['ktp'];
            $supplier['toko'] = $post['toko'];
            $result = $this->t_supplier->validate_add($supplier);
            if($result===true) redirect(site_url('main/supplier'));
            else{
                $data['msg'] = $result;
                $data['prefill'] = $post;
            }
        }
        $data['viewer'] = 'supplier/form';
        $data['menu'] = 6;
        $this->load->view('main',$data);
    }
    
    function supplieredit($id){
        $supplier['id'] = $id;
        $this->load->model('t_supplier');
        $post = $this->input->post();
        if($post){
            $supplier['nama'] = $post['nama'];
            $supplier['hp'] = $post['hp'];
            $supplier['ktp'] = $post['ktp'];
            $supplier['toko'] = $post['toko'];
            $result = $this->t_supplier->validate_update($supplier);
            if($result===true) redirect(site_url('main/supplier'));
            else{
                $data['msg'] = $result;
                $data['prefill'] = $post;
            }
        }else $data['prefill'] = $this->t_supplier->find($supplier);
        $data['viewer'] = 'supplier/form';
        $data['menu'] = 6;
        $this->load->view('main',$data);
    }
}